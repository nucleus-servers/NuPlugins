/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.essential;

import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.PluginDescriptionFile;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ModuleEssential extends Module {

  List<UUID> did = new ArrayList<>();

  public ModuleEssential() {
    super("Essential", new String[] {"plugins", "pl", "ver", "version", "?", "skript", "sk"});
  }

  @Override
  public void onCommand(Player p, String cmd, String[] args) {
    if ((cmd.equalsIgnoreCase("plugins")) || (cmd.equalsIgnoreCase("pl"))) {
      StringBuilder sb = new StringBuilder();

      for (String s : ModuleManager.getEnabledModulesName()) {
        sb.append(ChatColor.RESET + ", " + ChatColor.GREEN + s);
      }

      p.sendMessage(prefix("Server") + "Modules (" + ModuleManager.getEnabledModules().length
          + ") : " + sb.toString().substring(4));
      p.sendMessage(prefix("Server") + "You're probably trying to " + ChatFormats.accentColor
          + "find plugins" + ChatFormats.baseColor + ", eh?");
      p.sendMessage(prefix("Server") + "Sorry mate, but you're out of luck, you wanker");
      p.sendMessage(prefix("Server") + "These are " + ChatFormats.accentColor + "modules"
          + ChatFormats.baseColor + ", they're 'sub-plugins' that we " + ChatFormats.accentColor
          + "originally created for this server");
    }

    if ((cmd.equalsIgnoreCase("ver")) || (cmd.equalsIgnoreCase("version"))) {
      PluginDescriptionFile pdf = NuPluginBase.getInstance().getDescription();
      p.sendMessage(prefix("Server") + "Bukkit Version : " + Bukkit.getVersion());
      p.sendMessage(prefix("Server") + "Mod Name : " + pdf.getName());
      p.sendMessage(prefix("Server") + "Mod Version : " + pdf.getVersion());
      p.sendMessage(prefix("Server") + "Mod Author(s) : ");

      for (String author : pdf.getAuthors())
        p.sendMessage(prefix("Server") + "              " + author);
    }

    if (cmd.equalsIgnoreCase("skript") || cmd.equalsIgnoreCase("sk")) {
      if (did.contains(p.getUniqueId())) {
        p.sendMessage(prefix("Server")
            + "GET OF MY LAWN AND LEARN A REAL PROGRAMMING LANGUAGE YOU PIECE OF SHIT");
      } else {
        p.sendMessage(prefix("Server")
            + "You call this a fucking programming language? COBOL is even better than this shit!");

        did.add(p.getUniqueId());
      }
    }

    if (cmd.equalsIgnoreCase("?")) {
      p.chat("/help");
    }
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void deathMessage(PlayerDeathEvent e) {
    e.getEntity().sendMessage(prefix("Death") + e.getDeathMessage());
    e.setDeathMessage("");
  }

}
