/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.ingamemm;

import com.nucleus.id.bukkit.NuCommon.exceptions.DoesNotExistException;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public class ModuleInGameModuleManager extends Module {

  public ModuleInGameModuleManager() {
    super("Module Manager", new String[] {"mm", "modulemanager", "modulectl"});
  }

  @Override
  public void onCommand(Player p, String cmd, String[] args) throws DoesNotExistException {
    if (!p.hasPermission(Permissions.STAFF)) {
      p.sendMessage(prefix() + ChatFormats.accentColor + "You don't have permission to do this");
      return;
    }

    if (args.length >= 2) {
      Module m = ModuleManager.getModule(args[0]);

      if (m == null)
        throw new DoesNotExistException(args[0], "Module");

      if (args[1].equalsIgnoreCase("enable")) {
        enableModule(p, m);
        return;
      } else if (args[1].equalsIgnoreCase("disable")) {
        disableModule(p, m);
        return;
      } else if (args[1].equalsIgnoreCase("info")) {
        infoModule(p, m);
        return;
      } else if (args[1].equalsIgnoreCase("unload")) {
        unloadModule(p, m);
        return;
      }
    }

    displayHelp(p, cmd);
  }


  private void enableModule(Player p, Module m) {
    ModuleManager.enableModule(m);
    p.sendMessage(prefix() + ChatColor.GREEN + "Module '" + m.getName() + "' successfully enabled");
  }


  private void disableModule(Player p, Module m) {
    ModuleManager.disableModule(m);
    p.sendMessage(
        prefix() + ChatColor.GREEN + "Module '" + m.getName() + "' successfully disabled");
  }

  private void unloadModule(Player p, Module m) {
    ModuleManager.unloadModule(m);
    p.sendMessage(
        prefix() + ChatColor.GREEN + "Module '" + m.getName() + "' successfully unloaded");
  }

  private void infoModule(Player p, Module m) {
    p.sendMessage(prefix() + ChatFormats.accentColor + "Name " + ChatFormats.baseColor + ": "
        + ChatFormats.accentColor + m.getName());
    p.sendMessage(prefix() + ChatFormats.accentColor + "Commands " + ChatFormats.baseColor + ": ");

    for (String cmd : m.getCommands())
      p.sendMessage(prefix() + ChatFormats.accentColor + "         /" + cmd);
  }

  private void displayHelp(Player p, String cmd) {
    p.sendMessage(prefix() + ChatFormats.accentColor + "/" + cmd + " <module> enable"
        + ChatFormats.baseColor + " - " + ChatFormats.accentColor + "Enables the Module");
    p.sendMessage(prefix() + ChatFormats.accentColor + "/" + cmd + " <module> disable"
        + ChatFormats.baseColor + " - " + ChatFormats.accentColor + "Disables the Module");
    p.sendMessage(
        prefix() + ChatFormats.accentColor + "/" + cmd + " <module> info" + ChatFormats.baseColor
            + " - " + ChatFormats.accentColor + "Shows Information of the Module");
    p.sendMessage(prefix() + ChatFormats.accentColor + "/" + cmd + " <module> enable"
        + ChatFormats.baseColor + " - " + ChatFormats.accentColor + "Unloads the Module "
        + ChatColor.DARK_RED + ChatColor.BOLD + "WARNING! " + ChatFormats.accentColor
        + "Do not use this If you don't know what you're doing!");
  }

}
