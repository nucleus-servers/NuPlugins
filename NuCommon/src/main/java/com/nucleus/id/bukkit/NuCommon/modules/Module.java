/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules;

import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.exceptions.CommandException;
import com.nucleus.id.bukkit.NuCommon.utils.JarUtils;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;

import static com.nucleus.id.bukkit.NuCommon.formats.ChatFormats.prefixFormat;

public abstract class Module implements Listener {

  protected NuPluginBase plugin = NuPluginBase.getInstance();
  protected String name = "NO_NAME";
  protected String prefix;
  protected String[] commands = new String[0];
  protected boolean enabled = false;
  private File configFile;
  private FileConfiguration config;

  public Module(String name) {
    this.name = this.prefix = name;
  }

  public Module(String name, String[] commands) {
    this(name);
    this.commands = commands;
  }

  /**
   * This Method triggers when the module is enabling
   */
  public void onEnable() {}

  /**
   * This Method triggers when the module is disabling
   */
  public void onDisable() {}

  /**
   * This Method triggers when the player uses a command from this module
   *
   * @param sender The Player who sent the command
   * @param cmd The Command that is being sent
   * @param args The Arguments for the command
   */
  public void onCommand(Player sender, String cmd, String[] args) throws CommandException {}

  /**
   * Returns The Module's Name
   *
   * @return The Name of this module
   */
  public String getName() {
    return name;
  }

  /**
   * Sets The Module's Name
   */
  protected void setName(String name) {
    this.name = name;
  }

  /**
   * Returns an Array of String filled with commands that are accepted by this module
   *
   * @return An Array of Commands that are accepted by this module
   */
  public String[] getCommands() {
    return this.commands;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  public FileConfiguration getConfig() {
    return config;
  }

  protected String prefix(String text) {
    return prefixFormat(text);
  }

  protected String prefix() {
    return prefix(name);
  }

  protected void log(Level l, String msg) {
    plugin.getLogger().log(l, "[" + name + "] " + msg);
  }

  protected NuPluginBase getPlugin() {
    return plugin;
  }

  protected void loadConfig() {
    if (configFile == null)
      configFile =
          new File(plugin.getDataFolder(), getName().toLowerCase().replace(' ', '_') + ".yml");

    config = new YamlConfiguration();

    try {
      config.load(configFile);
    } catch (Exception e) {
      log(Level.SEVERE, "Error while loading Config File for " + name);
      e.printStackTrace();
    }
  }

  protected void saveDefaultConfig() {
    if (configFile == null)
      configFile =
          new File(plugin.getDataFolder(), getName().toLowerCase().replace(' ', '_') + ".yml");

    if (!configFile.exists()) {
      try {
        JarUtils.extractFromJar(configFile.getName(), configFile.getAbsolutePath());
      } catch (IOException e) {
        throw new RuntimeException("Unable to save default config ", e);
      }
    }
  }

  public void saveConfig() {
    if (configFile == null)
      configFile = new File(plugin.getDataFolder(), name.toLowerCase().replace(' ', '_') + ".yml");

    try {
      config.save(configFile);
    } catch (IOException e) {
      log(Level.SEVERE, "Error while saving Config File for " + name);
      e.printStackTrace();
    }
  }

  protected void sendMessage(Player player, String message) {
    this.getPlugin().getServer().getPlayer(player.getUniqueId())
        .sendMessage(prefix(prefix) + message);
  }
}
