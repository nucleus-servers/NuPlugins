/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateTimeUtils {
  private static Calendar cal;
  private static Date date;

  static {
    cal = GregorianCalendar.getInstance();
    date = new Date();
  }

  public static int getHourOfDay() {
    refresh();
    return cal.get(Calendar.HOUR_OF_DAY);
  }

  public static int getHour() {
    refresh();
    return cal.get(Calendar.HOUR);
  }

  public static int getMinute() {
    refresh();
    return cal.get(Calendar.MINUTE);
  }

  public static int getSecond() {
    refresh();
    return cal.get(Calendar.SECOND);
  }

  public static int getPeriod() {
    refresh();
    return cal.get(Calendar.AM_PM);
  }

  public static int getDayOfWeek() {
    refresh();
    return cal.get(Calendar.DAY_OF_WEEK);
  }

  public static String getDayOfWeekName() {
    switch (getDayOfWeek()) {
      case Calendar.SUNDAY:
        return "Sunday";
      case Calendar.MONDAY:
        return "Monday";
      case Calendar.TUESDAY:
        return "Tuesday";
      case Calendar.WEDNESDAY:
        return "Wednesday";
      case Calendar.THURSDAY:
        return "Thursday";
      case Calendar.FRIDAY:
        return "Friday";
      case Calendar.SATURDAY:
        return "Saturday";
      default:
        return null;
    }
  }

  public static int getDayOfMonth() {
    refresh();
    return cal.get(Calendar.DAY_OF_MONTH);
  }

  public static int getDayOfYear() {
    refresh();
    return cal.get(Calendar.DAY_OF_YEAR);
  }

  public static int getMonth() {
    refresh();
    return cal.get(Calendar.MONTH);
  }

  public static int getYear() {
    refresh();
    return cal.get(Calendar.YEAR);
  }

  public static Date getDate() {
    refresh();
    return date;
  }

  private static void refresh() {
    cal.setTime(date = new Date());
  }
}
