/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.staffcall;

import java.util.UUID;

public class StaffCall {
  private UUID requester;
  private String message;
  private boolean attend = false;

  private UUID staffAttendee;

  public StaffCall(UUID requester) {
    this.requester = requester;
  }

  public StaffCall(UUID requester, String message) {
    this.requester = requester;
    this.message = message;
  }

  public void attend(UUID staffAttendee) {
    this.attend = true;
    this.staffAttendee = staffAttendee;
  }

  public boolean isAttend() {
    return attend;
  }

  public UUID getRequester() {
    return requester;
  }

  public UUID getStaff() {
    return staffAttendee;
  }

  public String getMessage() {
    return message;
  }
}
