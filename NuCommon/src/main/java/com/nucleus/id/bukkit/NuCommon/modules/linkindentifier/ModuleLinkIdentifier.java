/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.linkindentifier;

import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ModuleLinkIdentifier extends Module {

  private final Pattern pattern =
      Pattern.compile("^(https?)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]");

  public ModuleLinkIdentifier() {
    super("Link Indentifier");
  }

  @EventHandler
  public void onChat(AsyncPlayerChatEvent e) {
    for (String s : e.getMessage().split(" ")) {
      Matcher m = pattern.matcher(s);

      if (m.find())
        new IdentifyLinkTask(s).runTaskAsynchronously(plugin);

    }
  }
}
