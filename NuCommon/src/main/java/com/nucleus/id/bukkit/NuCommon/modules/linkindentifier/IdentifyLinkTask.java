/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.linkindentifier;

import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.utils.HTTPUtils;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class IdentifyLinkTask extends BukkitRunnable {

  private final Pattern TITLE_TAG =
      Pattern.compile("\\<title>(.*)\\</title>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

  private String url;

  public IdentifyLinkTask(String url) {
    this.url = url;
  }

  @Override
  public void run() {
    try {
      String data = HTTPUtils.sendGet(url);
      Matcher m = TITLE_TAG.matcher(data);

      if (m.find())
        Bukkit.broadcastMessage(ChatFormats.accentColor + "Link" + ChatFormats.baseColor + "> ["
            + ChatFormats.accentColor + url + ChatFormats.baseColor + "] " + ChatFormats.accentColor
            + m.group(1).replaceAll("[\\s\\<>]+", " ").trim());

    } catch (IOException ignored) {
    }
  }

}
