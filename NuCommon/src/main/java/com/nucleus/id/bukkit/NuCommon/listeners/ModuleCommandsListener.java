/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.listeners;

import com.nucleus.id.bukkit.NuCommon.exceptions.CommandException;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModuleCommandsListener implements Listener {

  private final String prefix = ChatFormats.prefixFormat("Server");

  @EventHandler
  public void onCommand(PlayerCommandPreprocessEvent e) {
    String[] messages = e.getMessage().split(" ");

    List<String> argList = new ArrayList<>(Arrays.asList(messages));
    argList.remove(0);

    String[] args = argList.toArray(new String[argList.size()]);

    for (Module module : ModuleManager.getEnabledModules()) {
      for (String command : module.getCommands()) {
        if (command.equalsIgnoreCase(messages[0].substring(1))) {
          try {
            module.onCommand(e.getPlayer(), command, args);
          } catch (CommandException ex) {
            for (String s : ex.getMessage().split("\n"))
              e.getPlayer().sendMessage(prefix + ChatFormats.accentColor + s);
          } catch (Exception ex) {
            ex.printStackTrace();
            e.getPlayer().sendMessage(prefix + ChatFormats.accentColor + "A(n) " + ex
                + " has occured. Please report the administrator(s)");
          }
          e.setCancelled(true);
          return;
        }
      }
    }
  }

}
