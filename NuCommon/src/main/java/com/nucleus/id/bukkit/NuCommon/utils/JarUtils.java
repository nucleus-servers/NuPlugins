/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.utils;

import com.google.common.io.ByteStreams;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

public class JarUtils {
  private static boolean RUNNING_FROM_JAR = false;

  static {
    final URL resource = JarUtils.class.getClassLoader().getResource("plugin.yml");

    if (resource != null)
      RUNNING_FROM_JAR = true;
  }

  public static JarFile getRunningJar() throws IOException {
    if (!RUNNING_FROM_JAR)
      return null;

    String path =
        new File(JarUtils.class.getProtectionDomain().getCodeSource().getLocation().getPath())
            .getAbsolutePath();

    path = URLDecoder.decode(path, "UTF-8");

    return new JarFile(path);
  }

  public static boolean extractFromJar(final String fileName, final String dest)
      throws IOException {
    if (getRunningJar() == null)
      return false;

    final File file = new File(dest);

    if (file.isDirectory()) {
      file.mkdir();
      return false;
    }

    if (!file.exists())
      file.getParentFile().mkdirs();

    final JarFile jar = getRunningJar();
    final Enumeration<JarEntry> e = jar.entries();

    while (e.hasMoreElements()) {
      final JarEntry je = e.nextElement();

      if (!je.getName().contains(fileName))
        continue;

      final InputStream in = new BufferedInputStream(jar.getInputStream(je));
      final OutputStream out = new BufferedOutputStream(new FileOutputStream(file));

      ByteStreams.copy(in, out);

      jar.close();
      return true;
    }

    jar.close();
    return false;
  }

  public static URL getJarUrl(final File file) throws IOException {
    return new URL("jar:" + file.toURI().toURL().toExternalForm() + "!/");
  }

}
