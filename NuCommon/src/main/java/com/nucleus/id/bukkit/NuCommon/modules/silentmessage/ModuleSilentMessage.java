/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.silentmessage;

import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class ModuleSilentMessage extends Module {

  public ModuleSilentMessage() {
    super("Silent Message");
  }

  @EventHandler
  public void onJoin(PlayerJoinEvent e) {
    e.setJoinMessage("");
  }

  @EventHandler
  public void onQuit(PlayerQuitEvent e) {
    e.setQuitMessage("");
  }
}
