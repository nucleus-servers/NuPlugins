/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.managers;

import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.event.HandlerList;

import java.util.ArrayList;
import java.util.Iterator;

public class ModuleManager {

  private static ArrayList<Module> modules = new ArrayList<Module>();
  private static NuPluginBase plugin = NuPluginBase.getInstance();

  public static void loadModule(Module m) {
    modules.add(m);
  }

  public static void loadModule(Class<Module> moduleClass) {
    try {
      Module module = moduleClass.newInstance();
      loadModule(module);
    } catch (Exception e) {
      logSevere("Error occured while loading a module! " + e);
      return;
    }
  }

  public static void loadModules(Class<Module>[] moduleClasses) {
    for (Class<Module> c : moduleClasses) {
      loadModule(c);
    }
  }

  public static Module getModule(String moduleName) {
    for (Module module : modules) {
      if (module.getName().equalsIgnoreCase(moduleName)) {
        return module;
      }
    }

    return null;
  }

  public static Module getModule(Class<Module> moduleClass) {
    for (Module module : modules) {
      if (module.getClass().equals(moduleClass)) {
        return module;
      }
    }

    return null;
  }

  public static void unloadModule(Module module) {
    if (module.isEnabled()) {
      disableModule(module);
    }

    modules.remove(module);
  }

  public static void unloadModule(String moduleName) throws NullPointerException {
    Module module = getModule(moduleName);

    if (module == null) {
      throw new NullPointerException("Module does not exist or not loaded!");
    }

    unloadModule(module);
  }

  public static void unloadModule(Class<Module> moduleClass) throws NullPointerException {
    Module module = getModule(moduleClass);

    if (module == null) {
      throw new NullPointerException("Module does not exist or not loaded!");
    }

    unloadModule(module);
  }

  public static void unloadAllModules() {
    Iterator<Module> it = modules.iterator();

    while (it.hasNext()) {
      Module m = it.next();

      if (m.isEnabled()) {
        disableModule(m);
      }

      it.remove();
    }
  }

  public static void enableModule(Module module) {
    if (module.isEnabled()) {
      return;
    }

    module.onEnable();
    module.setEnabled(true);

    plugin.getServer().getPluginManager().registerEvents(module, plugin);
  }

  public static void enableModule(String moduleName) throws NullPointerException {
    Module module = getModule(moduleName);

    if (module == null) {
      throw new NullPointerException("Module does not exist or not loaded!");
    }

    enableModule(module);
  }

  public static void enableModule(Class<Module> moduleClass) throws NullPointerException {
    Module module = getModule(moduleClass);

    if (module == null) {
      throw new NullPointerException("Module does not exist or not loaded!");
    }

    enableModule(module);
  }

  public static void enableAllModules() {
    for (Module module : modules) {
      enableModule(module);
    }
  }

  public static void disableModule(Module module) {
    if (!module.isEnabled()) {
      return;
    }

    module.onDisable();
    module.setEnabled(false);

    HandlerList.unregisterAll(module);
  }

  public static void disableModule(String moduleName) throws NullPointerException {
    Module module = getModule(moduleName);

    if (module == null) {
      throw new NullPointerException("Module does not exist or not loaded!");
    }

    disableModule(module);
  }

  public static void disableModule(Class<Module> moduleClass) throws NullPointerException {
    Module module = getModule(moduleClass);

    if (module == null) {
      throw new NullPointerException("Module does not exist or not loaded!");
    }

    disableModule(module);
  }

  public static void disableAllModules() {
    for (Module module : modules) {
      disableModule(module);
    }
  }

  public static Module[] getModules() {
    return modules.toArray(new Module[modules.size()]);
  }

  public static Module[] getEnabledModules() {
    ArrayList<Module> enabledModules = new ArrayList<Module>();

    for (Module module : modules) {
      if (module.isEnabled()) {
        enabledModules.add(module);
      }
    }

    return enabledModules.toArray(new Module[enabledModules.size()]);
  }

  public static String[] getModulesName() {
    String[] names = new String[modules.size()];

    for (int i = 0; i < modules.size(); i++) {
      names[i] = modules.get(i).getName();
    }

    return names;
  }

  public static String[] getEnabledModulesName() {
    String[] names = new String[getEnabledModules().length];

    for (int i = 0; i < getEnabledModules().length; i++) {
      names[i] = getEnabledModules()[i].getName();
    }

    return names;
  }

  private static void logSevere(String msg) {
    plugin.getLogger().severe("[ModuleManager] " + msg);
  }
}
