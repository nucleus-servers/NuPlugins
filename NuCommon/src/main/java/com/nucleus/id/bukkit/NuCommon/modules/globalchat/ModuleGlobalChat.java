/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.globalchat;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;

import com.nucleus.id.bukkit.NuCommon.modules.Module;

import net.md_5.bungee.api.ChatColor;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import java.util.logging.Level;

public class ModuleGlobalChat extends Module {

  public ModuleGlobalChat() {
    super("Global Chat");
  }

  @Override
  public void onEnable() {
    Bukkit.getMessenger().registerOutgoingPluginChannel(plugin, "BungeeCord");
  }

  @EventHandler(priority = EventPriority.HIGHEST)
  public void onChat(AsyncPlayerChatEvent e) {
    if (e.isCancelled())
      return;

    ByteArrayDataOutput out = ByteStreams.newDataOutput();

    out.writeUTF("Nucleus");

    out.writeUTF("Chat");

    String message = String.format(e.getFormat(), e.getPlayer().getDisplayName(), e.getMessage());

    out.writeUTF(message);

    log(Level.INFO, ChatColor.stripColor(message));

    e.getPlayer().sendPluginMessage(plugin, "BungeeCord", out.toByteArray());

    e.setCancelled(true);
  }
}
