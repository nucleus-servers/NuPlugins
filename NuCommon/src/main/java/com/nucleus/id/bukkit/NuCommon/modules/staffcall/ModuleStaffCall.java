/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.staffcall;

import com.nucleus.id.bukkit.NuCommon.exceptions.CommandException;
import com.nucleus.id.bukkit.NuCommon.exceptions.DoesNotExistException;
import com.nucleus.id.bukkit.NuCommon.exceptions.NoPermissionException;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class ModuleStaffCall extends Module {

  ArrayList<StaffCall> calls = new ArrayList<>();

  public ModuleStaffCall() {
    super("Staff Call",
        new String[] {"call", "callstaff", "helpme", "helpop", "attend", "calllist", "close"});
  }

  @Override
  public void onCommand(Player sender, String cmd, String[] args) throws CommandException {
    switch (cmd) {
      case "attend":
        if (!sender.hasPermission(Permissions.STAFF))
          throw new NoPermissionException();

        if (args.length >= 1) {
          StaffCall call = findCall(Bukkit.getPlayer(args[0]));

          if (call != null)
            if (!call.isAttend())
              attend(sender, call);
            else
              throw new CommandException(
                  "The call is already attended by " + Bukkit.getPlayer(call.getStaff()).getName());
          else
            throw new DoesNotExistException(args[0], "Call");
        } else {
          for (int i = 0; i > calls.size(); i++)
            if (!calls.get(i).isAttend())
              attend(sender, calls.get(i));
        }
        break;
      case "calllist":
        if (!sender.hasPermission(Permissions.STAFF))
          throw new NoPermissionException();

        for (StaffCall call : calls)
          sender.sendMessage(Bukkit.getPlayer(call.getRequester()).getDisplayName());
        break;
      case "close":
        StaffCall call = findCall(sender);

        if (call == null)
          throw new CommandException("You didn't create a call");

        if (call.getStaff() != null)
          Bukkit.getPlayer(call.getStaff())
              .sendMessage(prefix() + "The Call you're attending is now closed");

        calls.remove(call);
        break;
      default:
        if (getStaffAmount() == 0)
          throw new CommandException("There's no available staff");

        String message = null;

        if (args.length >= 1) {
          StringBuilder sb = new StringBuilder();

          for (String s : Arrays.asList(args))
            sb.append(s).append(" ");

          message = sb.toString();
        }

        call(sender, message);
        break;
    }
  }

  private void attend(Player sender, StaffCall call) {
    call.attend(sender.getUniqueId());

    sender.teleport(Bukkit.getPlayer(call.getRequester()));

    sender.sendMessage(prefix() + Bukkit.getPlayer(call.getRequester()).getDisplayName()
        + ChatFormats.baseColor + " created a call"
        + (call.getMessage() != null ? " with the message \"" + call.getMessage() + "\"" : ""));
  }

  private void call(Player sender, String message) {
    if (isPlayerExist(sender.getUniqueId())) {
      sender.sendMessage(prefix() + ChatFormats.accentColor + "You've already created a call");
      return;
    }

    if (message == null)
      calls.add(new StaffCall(sender.getUniqueId()));
    else
      calls.add(new StaffCall(sender.getUniqueId(), message));

    broadcastStaff(prefix() + sender.getDisplayName() + ChatFormats.baseColor + " created a call"
        + (message != null ? " with the message \"" + message + "\"" : "")
        + ". To Attend Please use " + ChatFormats.accentColor + "/attend [playername]");
  }

  private void broadcastStaff(String message) {
    for (Player p : Bukkit.getOnlinePlayers()) {
      if (p.hasPermission(Permissions.MOD)) {
        if (message.contains("\n")) {
          for (String s : message.split("\n")) {
            p.sendMessage(prefix() + s);
          }
        } else {
          p.sendMessage(prefix() + message);
        }
      }
    }
  }

  private StaffCall findCall(Player requester) {
    for (StaffCall call : calls)
      if (call.getRequester().equals(requester.getUniqueId()))
        return call;
    return null;
  }

  private int getStaffAmount() {
    int i = 0;
    for (Player p : Bukkit.getOnlinePlayers())
      if (p.hasPermission(Permissions.STAFF))
        i++;
    return i;
  }

  private boolean isPlayerExist(UUID id) {
    for (StaffCall call : calls)
      if (call.getRequester().equals(id))
        return true;
    return false;
  }
}

