/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.utils;

public class StringUtils {
  public static String buildString(String[] args, int startIndex, int endIndex) {
    StringBuilder sb = new StringBuilder();

    for (int i = startIndex; i < endIndex; i++)
      sb.append(args[i] + " ");

    return sb.toString().substring(0, sb.length() - 1);
  }

  public static String buildString(String[] args, int startIndex) {
    return buildString(args, startIndex, args.length);
  }

  public static String buildString(String[] args) {
    return buildString(args, 0);
  }
}
