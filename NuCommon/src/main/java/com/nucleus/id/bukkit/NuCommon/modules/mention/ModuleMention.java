/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon.modules.mention;

import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class ModuleMention extends Module {

  public ModuleMention() {
    super("Mention");
  }

  @EventHandler(priority = EventPriority.HIGH)
  public void onChat(AsyncPlayerChatEvent e) {
    if (e.isCancelled())
      return;

    boolean pinged = false;

    String[] msg = e.getMessage().split(" ");

    StringBuilder sb = new StringBuilder();

    for (String s : msg) {
      for (Player p : Bukkit.getOnlinePlayers()) {
        if (s.equalsIgnoreCase(p.getName())) {
          s = ChatColor.AQUA + p.getName() + ChatColor.RESET;
          if (!pinged) {
            pinged = true;

            p.playSound(p.getLocation(), Sound.LEVEL_UP, 5.0F, 1.0F);
            TitleAPI.sendActionBar(p,
                ChatFormats.baseColor + "You're " + ChatFormats.accentColor + "being mentioned"
                    + ChatFormats.baseColor + " by " + ChatFormats.accentColor
                    + e.getPlayer().getName());
          }
        }
      }
      sb.append(s + " ");
    }

    e.setMessage(sb.toString().substring(0, sb.toString().length() - 1));
  }

}
