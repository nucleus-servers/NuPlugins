/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuCommon;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.nucleus.id.bukkit.NuCommon.listeners.ModuleCommandsListener;
import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.modules.essential.ModuleEssential;
import com.nucleus.id.bukkit.NuCommon.modules.globalchat.ModuleGlobalChat;
import com.nucleus.id.bukkit.NuCommon.modules.greentext.ModuleGreenText;
import com.nucleus.id.bukkit.NuCommon.modules.hashtag.ModuleHashtag;
import com.nucleus.id.bukkit.NuCommon.modules.ingamemm.ModuleInGameModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.linkindentifier.ModuleLinkIdentifier;
import com.nucleus.id.bukkit.NuCommon.modules.mention.ModuleMention;
import com.nucleus.id.bukkit.NuCommon.modules.silentmessage.ModuleSilentMessage;
import com.nucleus.id.bukkit.NuCommon.modules.staffcall.ModuleStaffCall;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import java.beans.PropertyVetoException;
import java.io.File;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Logger;

public class NuPluginBase extends JavaPlugin {
  private static NuPluginBase instance;

  @SuppressWarnings("unchecked")
  private final Class<Module>[] moduleClasses =
      new Class[] {ModuleEssential.class, ModuleMention.class, ModuleInGameModuleManager.class,
          ModuleGreenText.class, ModuleLinkIdentifier.class, ModuleStaffCall.class,
          ModuleSilentMessage.class, ModuleHashtag.class, ModuleGlobalChat.class};

  @SuppressWarnings("unchecked")
  private final Class<Listener>[] listenerClasses = new Class[] {ModuleCommandsListener.class};

  protected Logger logger = getLogger();
  protected ComboPooledDataSource cpds;
  protected Economy economy;
  protected Permission permission;

  public static NuPluginBase getInstance() {
    return instance;
  }

  public void onEnable() {
    instance = this;

    logger.info("--- Enabling NuCommon ---");

    logger.info(">> Loading Configuration File ...");
    saveDefaultConfig();
    reloadConfig();

    logger.info(">> Starting SQL Connection ...");
    startDatabaseConnection();

    logger.info(">> Hooking into Vault ...");
    if (!setupEconomy()) {
      getLogger().severe(
          "Vault doesn't exist, Please Install Vault! http://dev.bukkit.org/bukkit-plugins/vault/");
      getLogger().severe("Suicide in Progress ...");
      this.setEnabled(false);
    }

    setupPermissions();

    logger.info(">> Loading Modules ...");
    ModuleManager.loadModules(moduleClasses);

    logger.info(">> Enabling Modules ...");
    ModuleManager.enableAllModules();

    logger.info(">> Registering Listeners ...");
    registerListeners();

    logger.info("-- NuCommon Enabled. Passing to Child Plugin --");
  }

  public void onDisable() {
    logger.info("--- Disabling NuCommon ---");

    logger.info(">> Unloading Modules ...");
    ModuleManager.unloadAllModules();

    logger.info(">> Closing SQL Connection ...");
    stopDatabaseConnection();

    logger.info("--- NuCommon Disabled. Passing to Child Plugin ---");
  }

  public Connection getDatabaseConnection() {
    try {
      return cpds.getConnection();
    } catch (SQLException e) {
      logger.severe("Error while getting connection from pool ...");
      e.printStackTrace();
      logger.severe("Suicide in Progress ...");
      this.setEnabled(false);
    }

    return null;
  }

  public Economy getEconomy() {
    return economy;
  }

  public Permission getPermission() {
    return permission;
  }

  private void startDatabaseConnection() {
    cpds = new ComboPooledDataSource("NuZombie");

    if (getConfig().getBoolean("mysql.use", false)) {
      // Use MySQL
      try {
        cpds.setDriverClass("com.mysql.jdbc.Driver");
      } catch (PropertyVetoException e) {
        logger.severe("Database driver error: " + e.getMessage());
      }

      cpds.setJdbcUrl("jdbc:mysql://" + getConfig().getString("mysql.host") + ":"
          + getConfig().getInt("mysql.port") + "/" + getConfig().getString("mysql.database"));
      cpds.setUser(getConfig().getString("mysql.username"));
      cpds.setPassword(getConfig().getString("mysql.password"));
    } else {
      // Use SQLite
      try {
        cpds.setDriverClass("org.sqlite.JDBC");
      } catch (PropertyVetoException e) {
        logger.severe("Database driver error: " + e.getMessage());
      }

      File dbFile = new File(getDataFolder(), "database.db");

      cpds.setJdbcUrl("jdbc:sqlite:" + dbFile.getAbsolutePath());
    }
  }

  private void stopDatabaseConnection() {
    cpds.close();
  }

  private boolean setupEconomy() {
    if (getServer().getPluginManager().getPlugin("Vault") == null) {
      return false;
    }

    RegisteredServiceProvider<Economy> rsp =
        getServer().getServicesManager().getRegistration(Economy.class);

    if (rsp == null)
      return false;

    economy = rsp.getProvider();

    return economy != null;
  }

  private boolean setupPermissions() {
    RegisteredServiceProvider<Permission> rsp =
        getServer().getServicesManager().getRegistration(Permission.class);
    permission = rsp.getProvider();
    return permission != null;
  }

  private void registerListeners() {
    for (Class<Listener> listenerClass : listenerClasses) {
      try {
        Bukkit.getPluginManager().registerEvents(listenerClass.newInstance(), this);
      } catch (Exception e) {
        logger.severe("An Exception occured while registering a listener " + e);
        e.printStackTrace();
      }
    }
  }
}
