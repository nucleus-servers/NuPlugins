/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.airdrop;

import com.nucleus.id.bukkit.NuCommon.utils.NumberUtils;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

/**
 * RunnableAirdrop is a runnable that checks if the player that is online is more than three, then
 * drop an AirDrop based on randomly chosen player from that server. With a constraint that the
 * player must be in the specified world.
 *
 * @see ModuleAirDrop#dropAirDrop(Player)
 */
public class RunnableAirDrop implements Runnable {

  ModuleAirDrop ad;

  public RunnableAirDrop(ModuleAirDrop airDrop) {
    this.ad = airDrop;
  }

  /**
   * Overrides the run method with the following:
   * <ul>
   * <li>Get players that are in the specified world</li>
   * <li>Checks if the number of players is less than three</li>
   * <li>Drops an AirDrop based on randomly chosen player</li>
   * </ul>
   *
   * If the amount of player is more then three, drop an AirDrop. Else, don't
   *
   * @see ModuleAirDrop#dropAirDrop(Player)
   */
  @Override
  public void run() {
    // instead of getting players that are online, get a list of players that are
    // in the specified world
    Player[] players = Bukkit.getWorld(ad.worldName).getPlayers()
        .toArray(new Player[Bukkit.getWorld(ad.worldName).getPlayers().size()]);

    if (players.length < 3)
      return;

    ad.dropAirDrop(players[NumberUtils.getRandomNumber(0, players.length - 1)]);
  }

}
