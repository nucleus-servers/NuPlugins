/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.weapondrop;

import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;
import com.nucleus.id.bukkit.NuZombie.utils.CrackShotUtils;
import com.shampaggon.crackshot.CSUtility;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Random;
import java.util.UUID;

public class ModuleWeaponDrop extends Module {

  Random rand = new Random();
  CSUtility cs;

  HashMap<UUID, Long> lastDropTime = new HashMap<>();

  public ModuleWeaponDrop() {
    super("Weapon Drop");
  }

  @Override
  public void onEnable() {
    cs = new CSUtility();
  }

  @EventHandler
  public void weaponDrop(PlayerRespawnEvent e) {
    Player p = e.getPlayer();

    if (!lastDropTime.containsKey(p.getUniqueId()))
      lastDropTime.put(p.getUniqueId(), 0L);

    if (System.currentTimeMillis() - lastDropTime.get(p.getUniqueId()) >= 7200 * 1000
        && rand.nextInt() % 8 == 0) {
      ItemStack item = CrackShotUtils.getRandomWeapon();

      p.getInventory().addItem(item);

      p.sendMessage(
          prefix() + ChatFormats.accentColor + "New Item Acquired! " + ChatFormats.baseColor
              + "You've found: " + ChatFormats.accentColor + cs.getWeaponTitle(item));

      TitleAPI.sendTitle(p, ChatFormats.accentColor + "New Item Acquired!", ChatFormats.baseColor
          + "You've found: " + ChatFormats.accentColor + cs.getWeaponTitle(item));

      lastDropTime.put(p.getUniqueId(), System.currentTimeMillis());
    }
  }


}
