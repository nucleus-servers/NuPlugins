/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.gunlogic;

import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.NumberUtils;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;
import com.shampaggon.crackshot.CSUtility;
import com.shampaggon.crackshot.events.WeaponHitBlockEvent;
import com.shampaggon.crackshot.events.WeaponPreShootEvent;
import com.shampaggon.crackshot.events.WeaponReloadEvent;
import com.shampaggon.crackshot.events.WeaponShootEvent;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.inventory.ItemStack;

public class ModuleGunLogic extends Module {

  private final short BASE_DAMAGE = 2;
  private final short EXPLOSION_DAMAGE = 15;
  private final short FIRE_DAMAGE = 10;
  private final short LIGHTNING_DAMAGE = 25;
  private final short LAVA_DAMAGE = FIRE_DAMAGE * 2;
  private CSUtility cs;

  public ModuleGunLogic() {
    super("Gun Logic");
  }

  @Override
  public void onEnable() {
    cs = new CSUtility();
  }

  @EventHandler
  public void onDamage(EntityDamageEvent e) {
    if (e.getEntity() instanceof Player) {
      Player p = (Player) e.getEntity();

      ItemStack i = p.getItemInHand();

      if (cs.getWeaponTitle(i) != null) {
        if (e.getCause() == DamageCause.BLOCK_EXPLOSION
            || e.getCause() == DamageCause.ENTITY_EXPLOSION) {
          i.setDurability((short) (i.getDurability() + EXPLOSION_DAMAGE));
        } else if (e.getCause() == DamageCause.FIRE || e.getCause() == DamageCause.FIRE_TICK) {
          i.setDurability((short) (i.getDurability() + FIRE_DAMAGE));
        } else if (e.getCause() == DamageCause.LIGHTNING) {
          i.setDurability((short) (i.getDurability() + LIGHTNING_DAMAGE));
        } else if (e.getCause() == DamageCause.LAVA) {
          i.setDurability((short) (i.getDurability() + LAVA_DAMAGE));
        }
      }
    }
  }

  @EventHandler
  public void onPreShoot(WeaponPreShootEvent e) {
    Player p = e.getPlayer();

    // Can't shoot while on water
    if (p.getLocation().getBlock().getType() == Material.STATIONARY_WATER
        || p.getLocation().getBlock().getType() == Material.WATER) {
      e.setCancelled(true);
      TitleAPI.sendActionBar(p,
          ChatFormats.baseColor + "You " + ChatFormats.accentColor + "can't shoot"
              + ChatFormats.baseColor + " while " + ChatFormats.accentColor + " swimming");
    }
  }

  @EventHandler
  public void onShoot(WeaponShootEvent e) {
    Player p = e.getPlayer();
    ItemStack i = e.getPlayer().getItemInHand();

    int damage = i.getDurability() + NumberUtils.getRandomNumber(0, BASE_DAMAGE);

    i.setDurability((short) damage);
  }

  @EventHandler
  public void onReload(WeaponReloadEvent e) {
    Player p = e.getPlayer();

    // Slower Reload while on water
    if (p.getLocation().getBlock().getType() == Material.STATIONARY_WATER
        || p.getLocation().getBlock().getType() == Material.WATER) {
      e.setReloadSpeed(e.getReloadSpeed() / 2);
      TitleAPI.sendActionBar(p,
          ChatFormats.baseColor + "Your " + ChatFormats.accentColor + "reload speed"
              + ChatFormats.baseColor + " is " + ChatFormats.accentColor
              + " slower while swimming");
    }
  }

  @EventHandler
  public void onHitBlock(WeaponHitBlockEvent e) {
    // Break Glass
    if (e.getBlock().getType() == Material.GLASS || e.getBlock().getType() == Material.THIN_GLASS
        || e.getBlock().getType() == Material.STAINED_GLASS
        || e.getBlock().getType() == Material.STAINED_GLASS_PANE)
      e.getBlock().breakNaturally();

    // Red Barrel meme
    if (e.getBlock().getType() == Material.TNT && e.getProjectile() instanceof Snowball) {
      e.getBlock().setType(Material.AIR);
      e.getBlock().getWorld().createExplosion(e.getBlock().getLocation(), 4);
    }
  }
}
