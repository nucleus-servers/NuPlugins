/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.yourmom;

import com.nucleus.id.bukkit.NuCommon.utils.DateTimeUtils;

public class PlayerChatStats {
  private volatile int timesInARow;
  private volatile int spamCount;
  private long lastSpam;
  private boolean disableAlert = false;

  public PlayerChatStats() {}

  public PlayerChatStats(int timesInARow, int spamCount) {
    this.timesInARow = timesInARow;
    this.spamCount = spamCount;
  }

  public int getTimesInARow() {
    return timesInARow;
  }

  public int getSpamCount() {
    return spamCount;
  }

  public boolean isNotified() {
    return !disableAlert;
  }

  public void incrementChat() {
    timesInARow++;
  }

  public void incrementSpam() {
    spamCount++;
  }

  public void resetChatCount() {
    timesInARow = 0;
  }

  public void resetSpamCount() {
    spamCount = 0;
  }

  public void toggleAlert() {
    disableAlert = !disableAlert;
  }

  public void updateLastSpam() {
    lastSpam = DateTimeUtils.getDate().getTime();
  }

  public long getCooldown() {
    return DateTimeUtils.getDate().getTime() - lastSpam;
  }

  public boolean canTalk() {
    return getCooldown() > this.spamCount * 5 * 60 * 1000;
  }
}
