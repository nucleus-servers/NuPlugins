/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.holdabletorch;

import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * ModuleHoldableTorch is a module that lights up the environment when a player is holding a torch
 */
public class ModuleHoldableTorch extends Module {

  public ModuleHoldableTorch() {
    super("Holdable Torch");
  }

  /**
   * Overrides current onEnable method so it schedules a repeating task that runs every 100 ticks or
   * 5 seconds, and gives every player Night Vision Potion Effect
   */
  @Override
  public void onEnable() {
    this.getPlugin().getServer().getScheduler().runTaskTimer(this.getPlugin(), () -> {
      Bukkit.getOnlinePlayers().stream().filter(p -> p.getItemInHand().getType() == Material.TORCH)
          .forEach(p -> p
              .addPotionEffect(new PotionEffect(PotionEffectType.NIGHT_VISION, 20 * 5, 1), true));
    } , 20 * 5L, 20 * 5L);
  }

}
