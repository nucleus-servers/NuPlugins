/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.moneykill;

import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.NumberUtils;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;
import com.nucleus.id.bukkit.NuZombie.modules.taxes.ModuleTaxes;

import net.milkbowl.vault.economy.Economy;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;

/**
 * ModuleMoneyKill is a module that gives player a money when the player kills a monster. In this
 * case, a zombie
 */
public class ModuleMoneyKill extends Module {
  private Economy econ;

  private double startTaxMoney;
  private double finishTaxMoney;

  private ModuleTaxes taxes;

  private FileConfiguration config;

  public ModuleMoneyKill() {
    super("Money Kill");

    config = NuPluginBase.getInstance().getConfig();
  }

  /**
   * Overrides the current onEnable method so it schedules a repeating task to determine current
   * available balance
   */
  @Override
  public void onEnable() {
    econ = plugin.getEconomy();

    taxes = (ModuleTaxes) ModuleManager.getModule("Taxes");

    this.plugin.getServer().getScheduler().runTaskTimerAsynchronously(this.getPlugin(), () -> {
      double updatedTaxMoney = taxes.getTaxBank();

      startTaxMoney = updatedTaxMoney + (finishTaxMoney - startTaxMoney);
      finishTaxMoney = startTaxMoney;

      config.set("taxesBank.currentBalance", startTaxMoney);
      NuPluginBase.getInstance().saveConfig();
    } , 200L, 1200L);
  }

  @EventHandler
  public void onEntityKill(EntityDeathEvent e) {
    if (e.getEntity().getKiller() != null) {
      Player p = e.getEntity().getKiller();

      if (p.getWorld().getName().equalsIgnoreCase("spawn"))
        return;

      if (e.getEntity() instanceof Monster) {
        if (finishTaxMoney > 0) {
          double money = finishTaxMoney * 0.001;
          finishTaxMoney -= money;
          econ.depositPlayer(p, money);

          String message = ChatFormats.baseColor + "You've received " + ChatFormats.accentColor
              + roundOff(money, 2) + " Nu" + ChatFormats.baseColor + " for killing "
              + ChatFormats.accentColor + e.getEntity().getName();

          p.sendMessage(prefix("Government") + message);

          TitleAPI.sendActionBar(p, ChatFormats.baseColor + "You've received "
              + ChatFormats.accentColor + roundOff(money, 2) + " Nu");
        }
      }
    }
  }

  @EventHandler
  public void onPlayerKill(PlayerDeathEvent e) {
    if (e.getEntity().getKiller() != null) {
      Player p = e.getEntity();
      Player killer = e.getEntity().getKiller();

      double moneyLost = econ.getBalance(p) * NumberUtils.getRandomNumber(10, 50) / 100;

      double tax = moneyLost * ModuleTaxes.TAX;

      moneyLost -= tax;

      econ.depositPlayer(killer, moneyLost);

      if (econ.getBalance(p) > moneyLost) {
        econ.withdrawPlayer(p, moneyLost);

        String message = ChatFormats.baseColor + "You've lost " + ChatFormats.accentColor
            + roundOff(moneyLost, 2) + " Nu" + ChatFormats.baseColor + " because You've been "
            + ChatFormats.accentColor + "killed" + ChatFormats.baseColor + " by "
            + ChatFormats.accentColor + killer.getDisplayName() + ChatFormats.baseColor + ".";

        p.sendMessage(prefix("Government") + message);

        TitleAPI.sendActionBar(p, ChatFormats.baseColor + "You've lost " + ChatFormats.accentColor
            + roundOff(moneyLost, 2) + " Nu");
      }

      taxes.addTaxBank(tax);
    }
  }

  private double roundOff(double value, int places) {
    return (double) Math.round(value * 10 * places) / 10 * places;
  }

}
