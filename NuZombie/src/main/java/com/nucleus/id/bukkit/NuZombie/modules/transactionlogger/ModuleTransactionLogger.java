/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.transactionlogger;


import com.Acrobot.ChestShop.Events.PreShopCreationEvent;
import com.Acrobot.ChestShop.Events.TransactionEvent;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;
import com.shampaggon.crackshot.CSUtility;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;
import org.wargamer2010.signshop.events.SSCreatedEvent;
import org.wargamer2010.signshop.events.SSPostTransactionEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ModuleTransactionLogger extends Module {
  private final static String WORLDNAME = "market";
  final long minUpdateTime = 1000L * 15L;
  long lastUpdate = 0L;

  CSUtility cs = new CSUtility();

  private List<Transaction> transactions = new ArrayList<>();

  public ModuleTransactionLogger() {
    super("Transaction Logger", new String[] {"translog", "tl"});
  }

  @Override
  public void onEnable() {
    this.getPlugin().getServer().getScheduler().runTaskTimerAsynchronously(this.getPlugin(),
        new RunnableDatabaseUpdater(this.getPlugin().getDatabaseConnection(), transactions),
        20L * 60L * 30L, 20L * 60L * 60L); // 30 mins after server startup, and every hour update
  }

  @Override
  public void onDisable() {
    this.getPlugin().getServer().getScheduler().runTask(this.getPlugin(),
        new RunnableDatabaseUpdater(this.getPlugin().getDatabaseConnection(), transactions));
  }

  @Override
  public void onCommand(Player sender, String cmd, String[] args) {
    if (!sender.hasPermission(Permissions.STAFF)) {
      sender.sendMessage(prefix() + ChatFormats.accentColor
          + "You don't have enough permissions to use this command");
      return;
    }

    if (System.currentTimeMillis() - lastUpdate >= minUpdateTime) {
      this.getPlugin().getServer().getScheduler().runTaskAsynchronously(this.getPlugin(),
          new RunnableDatabaseUpdater(this.getPlugin().getDatabaseConnection(), transactions));

      sender.sendMessage(prefix() + ChatColor.GREEN + "Your request has been sent");

      lastUpdate = System.currentTimeMillis();
    } else {
      sender.sendMessage(prefix() + ChatFormats.accentColor
          + "There's already a Transaction Update being requested!");
    }
  }


  @EventHandler
  public void onSignShopCreate(SSCreatedEvent e) {
    Material prevItem = e.getItems()[0].getType();
    for (ItemStack i : e.getItems()) {
      if (i.getType() != prevItem) {
        e.getPlayer().sendMessage(
            ChatFormats.accentColor + "You can only create shops with 1 type of item!");
        e.setCancelled(true);
        break;
      }
    }
    if (!e.getPlayer().getWorld().getName().equalsIgnoreCase(WORLDNAME)
        && !e.getPlayer().getPlayer().hasPermission(Permissions.STAFF)) {
      e.getPlayer().sendMessage(ChatFormats.accentColor + "You cannot create a shop in this world");
      e.setCancelled(true);
    }
  }

  // Only do this if the transaction already happened. Hence PostTransactionEvent
  @EventHandler
  public void onSignShopTransaction(SSPostTransactionEvent e) {
    if (e.getOperation().startsWith("i"))
      return;

    // @see http://dev.bukkit.org/bukkit-plugins/signshop/pages/sign-shop-api/
    UUID from = e.getShop().getOwner().getPlayer().getUniqueId();
    UUID to = e.getPlayer().getPlayer().getUniqueId();

    // don't buy your own shop
    if (!from.equals(to)) {
      // Since SignShop can create a shop with Multiple Items, we need a price for each stack of
      // item
      double itemPriceStack = e.getPrice() / e.getItems().length;

      // Iterate each stack
      for (ItemStack i : e.getItems()) {
        String itemName = getName(i);
        int itemAmount = i.getAmount();

        // Since, we have the price for each stack of items, we need the price per item
        double itemPrice = itemPriceStack / itemAmount;

        transactions.add(new Transaction(itemName, itemPrice, from, to));
      }
    }
  }

  @EventHandler
  public void onChestShopCreate(PreShopCreationEvent e) {
    if (!e.getPlayer().getWorld().getName().equalsIgnoreCase(WORLDNAME)
        && !e.getPlayer().hasPermission(Permissions.STAFF)) {
      e.setOutcome(PreShopCreationEvent.CreationOutcome.NO_PERMISSION_FOR_TERRAIN);
      e.getPlayer().sendMessage(ChatFormats.accentColor + "You cannot create a shop in this world");
    }
  }

  @EventHandler
  public void onChestShopTransaction(TransactionEvent e) {
    UUID from = e.getClient().getUniqueId();
    UUID to = e.getOwner().getUniqueId();

    // don't buy your own shop
    if (!from.equals(to)) {
      // get the amount of the item
      int itemAmount = e.getStock().length;

      // price per one item
      double itemPrice = e.getPrice() / itemAmount;

      // Since, ChestShop can't have different items in 1 shop, so yeah. we've saved some cpu cycles
      // here!
      String itemName = getName(e.getStock()[0]);

      transactions.add(new Transaction(itemName, itemPrice, from, to));
    }
  }

  private String getName(ItemStack item) {
    String itemName = cs.getWeaponTitle(item);

    if (itemName == null)
      itemName = item.getType().toString();

    return itemName;
  }
}
