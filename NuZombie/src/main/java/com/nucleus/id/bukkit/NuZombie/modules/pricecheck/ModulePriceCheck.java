/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.pricecheck;

import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;
import com.shampaggon.crackshot.CSUtility;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class ModulePriceCheck extends Module {

  final long minUpdateTime = 1000L * 15L;
  long lastUpdate = 0L;

  List<ItemPrice> prices = new ArrayList<>();

  CSUtility cs = new CSUtility();

  public ModulePriceCheck() {
    super("Price Check", new String[] {"pc", "price", "pricecheck", "worth"});
  }

  @Override
  public void onEnable() {
    this.getPlugin().getServer().getScheduler().runTaskTimerAsynchronously(this.getPlugin(),
        new RunnablePriceUpdate(this.getPlugin().getDatabaseConnection(), prices), 20L * 60L * 5L,
        20L * 60L * 60L); // Update after 5 mins server startup, Update every hour
  }

  @Override
  public void onCommand(Player sender, String cmd, String[] args) {
    if (args.length >= 1) {
      if (args[0].equalsIgnoreCase("check") && sender.hasPermission(Permissions.STAFF))
        requestUpdateCommand(sender);
      else
        checkPriceCommand(sender, args);
    } else {
      sender.sendMessage(prefix() + "Usage: ");
      sender.sendMessage(prefix() + ChatFormats.accentColor + "/" + cmd + " <item name> "
          + ChatFormats.baseColor + ":" + ChatFormats.accentColor + " Check Price of an Item");
      sender.sendMessage(
          prefix() + ChatFormats.accentColor + "/" + cmd + " hand " + ChatFormats.baseColor + ":"
              + ChatFormats.accentColor + " Check Price of an Item you're holding");
    }
  }

  private void requestUpdateCommand(Player p) {
    if (System.currentTimeMillis() - lastUpdate >= minUpdateTime) {
      this.getPlugin().getServer().getScheduler().runTaskAsynchronously(this.getPlugin(),
          new RunnablePriceUpdate(this.getPlugin().getDatabaseConnection(), prices));

      p.sendMessage(prefix() + ChatColor.GREEN + "Your request has been sent");

      lastUpdate = System.currentTimeMillis();
    } else {
      p.sendMessage(
          prefix() + ChatFormats.accentColor + "There's already a Price Check being requested!");
    }
  }

  private void checkPriceCommand(Player p, String[] args) {
    String itemName;
    Material mat;


    if (args[0].equalsIgnoreCase("hand"))
      itemName = p.getItemInHand().getType().toString();
    else
      itemName =
          ((mat = Material.matchMaterial(buildString(args, 0))) != null ? mat.toString() : null);

    if (itemName == null) {
      p.sendMessage(prefix() + ChatFormats.accentColor + "No such item exists!");

      return;
    }

    ItemPrice itemPrice = findPrice(itemName);

    if (itemPrice != null) {
      p.sendMessage(prefix() + "Price for '" + ChatFormats.accentColor + itemPrice.getName()
          + ChatFormats.baseColor + "' based on Transaction Statistics");
      p.sendMessage(prefix() + ChatFormats.accentColor + "Average" + ChatFormats.baseColor + " : "
          + ChatFormats.accentColor + itemPrice.getAverage() + " Nu");
      p.sendMessage(prefix() + ChatFormats.accentColor + "Median" + ChatFormats.baseColor + " : "
          + ChatFormats.accentColor + itemPrice.getMedian() + " Nu");
    } else {
      p.sendMessage(
          prefix() + "That item '" + ChatFormats.accentColor + itemName + ChatFormats.baseColor
              + "' isn't being transacted by anyone or the cache hasn't update, yet");
      p.sendMessage(prefix() + "Price of Gold is always 64 Nu");
    }
  }

  private ItemPrice findPrice(String name) {
    return prices.stream().filter(p -> p.getName().equalsIgnoreCase(name)).findFirst().orElse(null);
  }

  private String buildString(String[] args, int start) {
    StringBuilder sb = new StringBuilder();

    for (int i = start; i < args.length; i++)
      sb.append(args[i]).append(" ");

    return sb.toString().substring(0, sb.length() - 1);
  }

}
