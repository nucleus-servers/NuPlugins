/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie;

import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuZombie.modules.airdrop.ModuleAirDrop;
import com.nucleus.id.bukkit.NuZombie.modules.factionnotify.ModuleFactionNotify;
import com.nucleus.id.bukkit.NuZombie.modules.factiontag.ModuleFactionTag;
import com.nucleus.id.bukkit.NuZombie.modules.gameplay.ModuleGameplay;
import com.nucleus.id.bukkit.NuZombie.modules.gunlogic.ModuleGunLogic;
import com.nucleus.id.bukkit.NuZombie.modules.holdabletorch.ModuleHoldableTorch;
import com.nucleus.id.bukkit.NuZombie.modules.moneykill.ModuleMoneyKill;
import com.nucleus.id.bukkit.NuZombie.modules.pricecheck.ModulePriceCheck;
import com.nucleus.id.bukkit.NuZombie.modules.punishment.ModulePunishment;
import com.nucleus.id.bukkit.NuZombie.modules.ranks.ModuleRanks;
import com.nucleus.id.bukkit.NuZombie.modules.taxes.ModuleTaxes;
import com.nucleus.id.bukkit.NuZombie.modules.transactionlogger.ModuleTransactionLogger;
import com.nucleus.id.bukkit.NuZombie.modules.weapondrop.ModuleWeaponDrop;

public class NuPlugin extends NuPluginBase {


  @SuppressWarnings("unchecked")
  private Class<Module>[] moduleClasses =
      new Class[] {ModuleAirDrop.class, ModuleRanks.class, ModuleGunLogic.class,
          ModuleWeaponDrop.class, ModuleTransactionLogger.class, ModulePriceCheck.class,
          ModuleTaxes.class, ModuleGameplay.class, ModuleMoneyKill.class, ModulePunishment.class,
          ModuleFactionTag.class, ModuleFactionNotify.class, ModuleHoldableTorch.class};

  @Override
  public void onEnable() {
    super.onEnable();

    logger.info("-- Enabling NuZombie --");

    logger.info(">> Loading Modules ...");
    ModuleManager.loadModules(moduleClasses);

    logger.info(">> Enabling Modules ...");
    ModuleManager.enableAllModules();

    logger.info("-- NuZombie Enabled. Enjoy! --");
  }


}
