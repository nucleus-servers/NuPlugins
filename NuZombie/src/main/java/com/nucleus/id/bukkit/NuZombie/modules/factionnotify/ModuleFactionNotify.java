package com.nucleus.id.bukkit.NuZombie.modules.factionnotify;

import com.google.common.base.Optional;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.event.FPlayerJoinEvent;
import com.massivecraft.factions.event.FPlayerLeaveEvent;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerMoveEvent;

public class ModuleFactionNotify extends Module {

  public ModuleFactionNotify() {
    super("Faction Notify");
  }

  @EventHandler
  public void gotoTerritory(PlayerMoveEvent e) {
    Player p = e.getPlayer();

    Optional<Faction> factionFrom = Optional.of(Board.getFactionAt(e.getFrom()));
    Optional<Faction> factionTo = Optional.of(Board.getFactionAt(e.getFrom()));

    if (!factionFrom.isPresent() || !factionTo.isPresent()) {
      Faction from = factionFrom.get();
      Faction to = factionTo.get();

      if (from != to) {
        if (to.getTag() != "Wilderness") {
          TitleAPI.sendActionBar(p, ChatFormats.baseColor + "You're now at "
              + ChatFormats.accentColor + to.getTag() + "'s Territory");
        } else {
          TitleAPI.sendActionBar(p, ChatFormats.baseColor + "You left " + ChatFormats.accentColor
              + from.getTag() + "'s Territory");
        }
      }
    }
  }

  @EventHandler
  public void playerJoin(FPlayerJoinEvent e) {
    e.getFaction().getFPlayers().forEach((FPlayer p) -> TitleAPI.sendActionBar(p.getPlayer(),
        ChatFormats.accentColor + e.getFPlayer().getPlayer().getName() + " joined your faction."));
  }

  @EventHandler
  public void playerLeave(FPlayerLeaveEvent e) {
    e.getFaction().getFPlayers().forEach((FPlayer p) -> TitleAPI.sendActionBar(p.getPlayer(),
        ChatFormats.accentColor + e.getFPlayer().getPlayer().getName() + " left your faction."));
  }

}
