/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.punishment;

import com.nucleus.id.bukkit.NuCommon.managers.ModuleManager;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuZombie.modules.taxes.ModuleTaxes;
import me.confuser.banmanager.events.PlayerBanEvent;
import net.milkbowl.vault.economy.Economy;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;

public class ModulePunishment extends Module {
  private Economy econ;

  private ModuleTaxes taxes;

  public ModulePunishment() {
    super("Punishment");
  }

  @Override
  public void onEnable() {
    econ = plugin.getEconomy();

    taxes = (ModuleTaxes) ModuleManager.getModule("Taxes");
  }

  @EventHandler
  public void takeMoney(PlayerBanEvent e) {
    Player p = e.getBan().getPlayer().getPlayer();
    double balance = econ.getBalance(p);

    econ.withdrawPlayer(p, balance);
    taxes.addTaxBank(balance);
  }

}
