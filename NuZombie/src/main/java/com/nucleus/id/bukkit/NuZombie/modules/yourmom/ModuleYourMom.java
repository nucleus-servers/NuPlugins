/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.yourmom;

import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;

public class ModuleYourMom extends Module {
  private final int spamRate = 5;
  private String lastChatter = null;
  private Map<String, PlayerChatStats> playerStats = new TreeMap<>();

  public ModuleYourMom() {
    super("YourMom", new String[] {"ymbot", "ym"});
  }

  @Override
  public void onEnable() {
    for (Player player : this.getPlugin().getServer().getOnlinePlayers()) {
      insertIfNull(player.getName());
    }
  }

  @EventHandler
  public void onPlayerJoin(PlayerJoinEvent e) {
    insertIfNull(e.getPlayer().getName());
  }

  @EventHandler
  public void onPlayerQuit(PlayerQuitEvent e) {
    String playername = e.getPlayer().getName();

    playerStats.get(playername).resetChatCount();
  }

  @EventHandler
  public void onPlayerChat(AsyncPlayerChatEvent e) {
    Player chatter = e.getPlayer();
    String currentChatter = chatter.getName();

    if (lastChatter == null) {
      lastChatter = currentChatter;
      playerStats.get(currentChatter).incrementChat();
      return;
    }

    PlayerChatStats chatStat = playerStats.get(currentChatter);

    if (lastChatter.equalsIgnoreCase(currentChatter)) {
      if (chatStat.canTalk()) {
        chatStat.incrementChat();
        if (chatStat.getTimesInARow() >= spamRate) {
          chatStat.resetChatCount();

          chatStat.incrementSpam();
          chatStat.updateLastSpam();

          throwCantTalk(chatter);
          e.setCancelled(true);
          log(Level.INFO, currentChatter + " is spamming. Amount: " + chatStat.getSpamCount());

          if (chatStat.getSpamCount() == 1) {
            chatter.sendMessage(prefix() + "You may disable future notifications by doing: /ym ta");
          }
          return;
        }
      } else {
        throwCantTalk(chatter);
        e.setCancelled(true);
        return;
      }
    } else {
      playerStats.values().stream().filter(pcs -> !pcs.equals(playerStats.get(currentChatter)))
          .forEach(PlayerChatStats::resetChatCount);
      chatStat.incrementChat();
      lastChatter = currentChatter;
      return;
    }
  }

  @Override
  public void onCommand(Player sender, String cmd, String[] args) {
    if (args.length >= 1) {
      if (sender.hasPermission(Permissions.STAFF)) {
        if (args[0].equalsIgnoreCase("check")) {
          if (args.length > 1) {
            showStats(args[1], sender);
          } else {
            showStats(sender.getName(), sender);
          }
        } else if (args[0].equalsIgnoreCase("last")) {
          sender.sendMessage(prefix("YourMom") + "last chatter is " + lastChatter);
        } else if (args[0].equalsIgnoreCase("resetspam")) {
          if (args.length > 1) {
            resetSpamCount(args[0]);
          } else {
            resetSpamCount(sender.getName());
          }
        }
      }
      if (args[0].equalsIgnoreCase("ta")) {
        PlayerChatStats pcs = playerStats.get(sender.getName());
        pcs.toggleAlert();

        if (pcs.isNotified()) {
          sender.sendMessage((prefix("YourMom") + "you will get notified from now"));
        } else {
          sender.sendMessage((prefix("YourMom") + "you won't get notified from now"));
        }
      }
    }
  }

  private void insertIfNull(String playerName) {
    if (playerStats.get(playerName) == null) {
      playerStats.put(playerName, new PlayerChatStats());
    }
  }

  private void resetSpamCount(String playername) {
    playerStats.get(playername).resetSpamCount();
  }

  private void showStats(String checkPlayer, Player sender) {
    try {
      PlayerChatStats msg_show = playerStats.get(checkPlayer);
      sender.sendMessage(prefix("YourMom") + "--- YourMom stats for " + checkPlayer + " ---");
      sender.sendMessage(prefix("YourMom") + "Current chat in a row: " + msg_show.getTimesInARow());
      sender.sendMessage(prefix("YourMom") + "Spam count: " + msg_show.getSpamCount());
      sender.sendMessage(prefix("YourMom") + "Cooldown (s): "
          + (msg_show.getSpamCount() * 5 * 60 - msg_show.getCooldown() / 1000));
      sender.sendMessage(prefix("YourMom") + "--- End of report ---");
    } catch (NullPointerException npe) {
      sender.sendMessage(
          prefix("YourMom") + "player " + checkPlayer + " was not found in this session.");
    }

  }

  private void throwCantTalk(Player chatter) {
    PlayerChatStats pcs = playerStats.get(chatter.getName());

    if (pcs.isNotified()) {
      int spamCount = pcs.getSpamCount();
      long seconds = (spamCount * 5 * 60) - (pcs.getCooldown() / 1000);

      StringBuilder cooldownTime = new StringBuilder();

      String hour = String.format("%02d", seconds / 60 / 60 / 24 % 24);
      String minute = String.format("%02d", seconds / 60 % 60);
      String second = String.format("%02d", seconds % 60);

      cooldownTime.append(hour + ":" + minute + ":" + second);

      chatter.sendMessage(
          prefix("YourMom") + "You can't talk publicly for another " + cooldownTime.toString());
    }
  }
}
