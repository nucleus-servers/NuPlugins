/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.transactionlogger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class RunnableDatabaseUpdater implements Runnable {

  private Connection database;
  private List<Transaction> transactions;

  public RunnableDatabaseUpdater(Connection database, List<Transaction> transactions) {
    this.database = database;
    this.transactions = transactions;
  }

  @Override
  public void run() {
    try {
      for (Transaction t : transactions) {
        PreparedStatement statement = database.prepareStatement(
            "INSERT INTO transactions (Item_Name, Price, Date, Seller, Buyer) VALUES (?,?,?,?,?)");

        statement.setString(1, t.getItemName());
        statement.setDouble(2, t.getPrice());
        statement.setDate(3, t.getDate());
        statement.setString(4, t.getSeller().toString());
        statement.setString(5, t.getBuyer().toString());

        statement.executeUpdate();
      }

      transactions.clear();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
