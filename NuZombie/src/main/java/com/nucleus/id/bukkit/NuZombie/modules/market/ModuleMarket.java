/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.market;

import com.intellectualcrafters.plot.api.PlotAPI;
import com.intellectualcrafters.plot.object.Plot;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.NumberUtils;
import com.plotsquared.bukkit.util.BukkitUtil;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.World;
import org.bukkit.entity.Player;

/**
 * ModuleMarket is a module that helps player finds another markets
 */
public class ModuleMarket extends Module {

  String worldName = "market";

  PlotAPI plotAPI;

  public ModuleMarket() {
    super("Market", new String[] {"market"});
  }

  @Override
  public void onEnable() {
    // Since we're only using this for getting plot locations, it's safe
    // @see
    // https://github.com/IntellectualSites/PlotSquared/blob/master/src/main/java/com/intellectualcrafters/plot/api/PlotAPI.java
    plotAPI = new PlotAPI();
  }

  @Override
  public void onCommand(Player p, String cmd, String[] args) {
    Plot targetPlot;

    if (args.length == 1) {
      OfflinePlayer ofPlayer = findPlayer(args[0]);

      if (ofPlayer == null) {
        p.sendMessage(prefix() + ChatFormats.accentColor + "Player doesn't exist!");
        return;
      }

      targetPlot = getRandomPlot(ofPlayer.getPlayer(), true);
    } else {
      targetPlot = getRandomPlot();
    }

    if (targetPlot == null) {
      p.sendMessage(prefix() + ChatFormats.accentColor
          + "There are no plot in the market / plot owned by the player");
      return;
    }

    p.teleport((Location) targetPlot.getHome().toBukkitLocation());
  }

  private void teleportPlayer(Player p, Plot plot) {
    if (p.getWorld().getName().equalsIgnoreCase(worldName)) {
      p.teleport(BukkitUtil.getLocation(plot.getHome()));
    } else {
      p.sendMessage(prefix() + "You'll be teleported in " + ChatFormats.accentColor + "5"
          + ChatFormats.baseColor + " seconds!");
      this.getPlugin().getServer().getScheduler().scheduleSyncDelayedTask(this.getPlugin(),
          () -> p.teleport(BukkitUtil.getLocation(plot.getHome())), 20L * 5L);
    }
  }

  private Plot getRandomPlot() {
    Plot[] plots = plotAPI.getPlots(getWorld());

    if (plots.length == 0)
      return null;

    return plots[NumberUtils.getRandomNumber(0, plots.length - 1)];
  }

  private Plot getRandomPlot(Player p, boolean owner) {
    Plot[] plots = plotAPI.getPlots(getWorld(), p, owner);

    if (plots.length == 0)
      return null;

    return plots[NumberUtils.getRandomNumber(0, plots.length - 1)];
  }

  private OfflinePlayer findPlayer(String name) {
    return this.getPlugin().getServer().getOfflinePlayer(name);
  }

  private World getWorld() {
    return Bukkit.getWorld(worldName);
  }

}
