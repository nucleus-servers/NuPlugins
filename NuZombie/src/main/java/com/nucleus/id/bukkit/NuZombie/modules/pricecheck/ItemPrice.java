/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.pricecheck;

public class ItemPrice {
  private String name;
  private double average;
  private double median;

  public ItemPrice(String name, double average, double median) {
    this.name = name;
    this.average = average;
    this.median = median;
  }

  public String getName() {
    return name;
  }

  public double getAverage() {
    return average;
  }

  public double getMedian() {
    return median;
  }
}
