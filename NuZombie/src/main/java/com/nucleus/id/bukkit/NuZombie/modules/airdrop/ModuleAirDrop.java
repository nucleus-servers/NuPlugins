/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.airdrop;

import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.exceptions.InvalidArgumentsException;
import com.nucleus.id.bukkit.NuCommon.exceptions.NoPermissionException;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.MaterialUtils;
import com.nucleus.id.bukkit.NuCommon.utils.NumberUtils;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;
import com.nucleus.id.bukkit.NuZombie.utils.CrackShotUtils;
import com.shampaggon.crackshot.CSUtility;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 * AirDrop is a limited time supply that is given to the players every once in a while, to help
 * players survive and not miserably searching for resources while encountering many zombies
 * <p>
 * AirDrop works by searching for a location based on player's location. And drops the AirDrop near
 * that location
 * </p>
 * <p>
 * When an AirDrop has dropped, it notifies players near that location and the distance to reach the
 * AirDrop. Players are given a certain number of time before the AirDrop disappears itself because
 * a new AirDrop is being spawned
 * </p>
 * <p>
 * The contents of the AirDrop is 20% weapon, 20% ammo, and 60% other supplies that may help player
 * survive
 * </p>
 */

public class ModuleAirDrop extends Module {
  protected static final String worldName = "world";

  private static final int intensity = 2; // 1 being a lot and 3 being normal
  private static final long duration = 20L * 60L * ((long) (intensity * 3.5)); // Too rare. Make it
                                                                               // more on January
                                                                               // 2016

  private static final int minRange = 216;
  private static final int maxRange = 1024;

  private static final int maxRadius = 3000;

  CSUtility cs;

  private Location old_ad =
      new Location(NuPluginBase.getInstance().getServer().getWorld(worldName), 0.0, 0.0, 0.0);

  /**
   * Assign command /airdrop and /ad to this module
   */
  public ModuleAirDrop() {
    super("AirDrop", new String[] {"airdrop", "ad"});
  }

  /**
   * Overrides the current onEnable() method and replaces it with the following:
   * <ul>
   * <li>Creates a new CSUtility object and assign it to the reference variable cs</li>
   * <li>Creates a new scheduled repeating task that runs every <b>duration</b> minute(s), creating
   * a new RunnableAirDrop every time it repeats</li>
   * </ul>
   */
  @Override
  public void onEnable() {
    cs = new CSUtility();

    this.getPlugin().getServer().getScheduler().runTaskTimer(this.getPlugin(),
        new RunnableAirDrop(this), duration, duration);
  }

  /**
   * Overrides onCommand method with the following: /ad,airdrop drophere. With a constraint that the
   * sender must has a permission of Permissions.STAFF
   *
   * @param sender The Player who sent the command
   * @param cmd The Command that is being sent
   * @param args The Arguments for the command
   */
  @Override
  public void onCommand(Player sender, String cmd, String[] args)
      throws NoPermissionException, InvalidArgumentsException {
    if (!sender.hasPermission(Permissions.STAFF))
      throw new NoPermissionException();

    if (args.length >= 1) {
      Location loc = sender.getLocation().clone();
      loc.setY(loc.getWorld().getHighestBlockYAt(loc));

      if (args[0].equalsIgnoreCase("drophere")) {
        spawnAirDrop(loc);
        sender.sendMessage(prefix("AirDrop") + "An Airdrop has been spawned at your Location");
      }
    } else {
      throw new InvalidArgumentsException("/" + cmd + " <drophere>");
    }
  }

  /**
   * Determine a valid AirDrop location based on the player's location. The location of the AirDrop
   * must not under water. Perhaps this method should've been named "determineAndSpawnAirDrop" to
   * prevent confusion.
   *
   * @param player the targeted player to spawn an AirDrop
   * @see #spawnAirDrop(Location)
   */
  public void dropAirDrop(Player player) {
    int offsetX = NumberUtils.getRandomNumber(minRange, maxRange);
    int offsetZ = NumberUtils.getRandomNumber(minRange, maxRange);

    // I bet someone is going to point out passing-int-into-the-function-that-requires-double thing

    Location point = player.getLocation().clone().add(offsetX, 0, offsetZ);

    // Because someone is obviously going to throw away their AirDrop on water
    if (point.getWorld().getHighestBlockAt(point).getType() == Material.WATER
        || point.getWorld().getHighestBlockAt(point).getType() == Material.STATIONARY_WATER)
      return;

    point.setY(point.getWorld().getHighestBlockYAt(point));

    spawnAirDrop(point);
  }

  /**
   * Drops current AirDrop at the specified location.
   *
   * @param loc the location that the AirDrop is going to drop
   */
  private void spawnAirDrop(Location loc) {
    Block b = loc.getWorld().getBlockAt(loc);

    b.setType(Material.CHEST);

    Chest c = (Chest) b.getState();

    fillAirDrop(c);

    broadcastAirDropNear(loc);
    removeOldAirDrop();

    old_ad = loc;
  }

  /**
   * Fills the AirDrop with supplies, with 20% chance of getting weapon, 20% chance of getting ammo
   * and 60% chance of getting other supplies The AirDrop fills at least 7 times to a random slot.
   * Slot overriding may happen.
   *
   * @param c the chest to be filled with
   */
  private void fillAirDrop(Chest c) {
    Inventory inv = c.getBlockInventory();

    for (int i = 0; i < NumberUtils.getRandomNumber(7, 26); i++) {
      // chance of getting a weapon and an ammo is 20%. This makes AirDrop worth finding
      int n = NumberUtils.getRandomNumber(1, 5);
      if (n == 5) {
        ItemStack wp = CrackShotUtils.getRandomWeapon();
        wp.setDurability((short) NumberUtils.getRandomNumber(0, 100));

        inv.setItem(NumberUtils.getRandomNumber(0, 26), wp);
      } else if (n == 4) {
        inv.setItem(NumberUtils.getRandomNumber(0, 26),
            new ItemStack(CrackShotUtils.getRandomAmmo()));
      } else {
        inv.setItem(NumberUtils.getRandomNumber(0, 26),
            new ItemStack(MaterialUtils.getRandomMaterial(), NumberUtils.getRandomNumber(1, 2)));
      }
    }

    c.update();
  }

  /**
   * Broadcast to players about the dropped AirDrop. Player that is near the location is going to
   * get the exact dropped AirDrop coordinate.
   *
   * @param loc the location of the AirDrop
   */
  private void broadcastAirDropNear(Location loc) {
    List<Player> nearPlayers = getPlayerNear(loc);
    for (Player player : Bukkit.getServer().getOnlinePlayers()) {
      if (nearPlayers.contains(player)) {
        int distance = (int) player.getLocation().distance(loc);

        String message = ChatFormats.baseColor + "An AirDrop has dropped at " + loc.getBlockX()
            + ", " + loc.getBlockY() + ", " + loc.getBlockZ() + " (" + ChatColor.BOLD
            + ChatColor.WHITE + "\u0394=" + distance + ChatFormats.baseColor + ")";

        player.sendMessage(prefix("AirDrop") + message);
        player.sendMessage(prefix("AirDrop") + "You have" + ChatFormats.accentColor + " 7 minutes "
            + ChatFormats.baseColor + "to find the AirDrop before it disappears");

        TitleAPI.sendActionBar(player, message);
      } else {
        String message = "You hear an AirDrop. It is really far away";

        player.sendMessage(prefix("AirDrop") + message);

        TitleAPI.sendActionBar(player, ChatFormats.baseColor + message);
      }
    }
    nearPlayers.clear();
  }

  /**
   * Removes the old AirDrop in case no one has ever found/searched it.
   */
  private void removeOldAirDrop() {
    try {
      Chest old_chest = (Chest) old_ad.getWorld().getBlockAt(old_ad).getState();
      old_chest.getBlockInventory().clear();

      old_ad.getWorld().getBlockAt(old_ad).setType(Material.AIR);
    } catch (ClassCastException cce) {
      log(Level.INFO, "Catched ClassCastException. No chest at that location. Already found?");
    }
  }

  /**
   * Gets a list of players that are near the specified location.
   *
   * @param loc the location
   * @return a list of players that are near the location
   */
  private List<Player> getPlayerNear(Location loc) {
    List<Player> playerList = new ArrayList<>();

    Bukkit.getServer().getOnlinePlayers().stream()
        .filter(player -> (player.getWorld().getName().equalsIgnoreCase(worldName))
            && (loc.distance(player.getLocation()) < maxRadius))
        .forEach(playerList::add);

    return playerList;
  }
}
