/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.ranks;

import com.nucleus.id.bukkit.NuCommon.exceptions.CommandException;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;

import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public class ModuleRanks extends Module {

  final String[] ranks = new String[] {"newbie", "adventurer", "hunter", "slayer", "wicked",
      "beast", "godlike", "beyondgodlike"};
  // do NOT let buff go below 1.0
  private final double BUFF = 1.0;
  Economy econ;
  Permission perm;
  Map<String, Double> ranksPrice = new HashMap<>();
  private DecimalFormat df = new DecimalFormat("#,###");

  public ModuleRanks() {
    super("Ranks", new String[] {"ranks", "rankup"});
  }

  @Override
  public void onEnable() {
    econ = plugin.getEconomy();
    perm = plugin.getPermission();

    updateRanksPrice();

    this.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(this.getPlugin(),
        this::updateRanksPrice, 200L, 20L * 60L * 10L);
  }

  private void updateRanksPrice() {
    ranksPrice.clear();
    for (int i = 1; i < ranks.length; i++) {
      int playerAmount = Bukkit.getServer().getOfflinePlayers().length;

      // Initial buff is adjustable. Adjust how you want. Default value is 3. Adjust wisely
      double modified_buff = (Math.cos(playerAmount) + BUFF) * 5;
      double price = 4000 * ((i * 2.5) + (modified_buff + 1) / 7);

      if (i >= 6) {
        price *= 1.5;
      } else if (i >= 4) {
        price *= 1.25;
      } else if (i >= 2) {
        price *= 1.15;
      }
      ranksPrice.put(ranks[i], price);
    }
  }

  @Override
  public void onCommand(Player sender, String cmd, String[] args) throws CommandException {
    int rankPos = getRankPosition(sender);

    if (getRankPosition(sender) == ranks.length - 1)
      throw new CommandException("You're already at the last rank");

    if (cmd.equals("rankup")) {
      if (rankPos < 0)
        throw new CommandException("You're a staff, seriously?");

      if (args.length == 1) {
        if (args[0].equalsIgnoreCase("buy")) {
          rankup(sender);
          return;
        }
      } else {
        rankupInfo(sender);
        return;
      }
    }

    if (cmd.equals("ranks"))
      ranks(sender);
  }

  private void rankupInfo(Player sender) {
    int playerRankPos = getRankPosition(sender);
    int targetRankPos = playerRankPos + 1;

    String oldRank = ranks[playerRankPos];
    String newRank = ranks[targetRankPos];

    double newRankPrice = ranksPrice.get(newRank);

    sender.sendMessage(ChatFormats.line());
    sender.sendMessage(" " + ChatFormats.accentColor + oldRank + ChatFormats.baseColor + " -> "
        + ChatFormats.accentColor + newRank);
    sender.sendMessage(
        " " + ChatFormats.baseColor + "Price : " + ChatFormats.accentColor + "$ " + newRankPrice);

    if (econ.getBalance(sender) >= newRankPrice)
      sender.sendMessage(" " + ChatFormats.baseColor + "Use " + ChatFormats.accentColor
          + "/rankup buy" + ChatFormats.baseColor + " to " + ChatFormats.accentColor + "upgrade"
          + ChatFormats.baseColor + "!");
    sender.sendMessage(ChatFormats.line());
  }

  private void rankup(Player sender) throws CommandException {
    if (getRankPosition(sender) == -1)
      throw new CommandException("You're a staff, seriously?");

    int playerRankPos = getRankPosition(sender);
    int targetRankPos = playerRankPos + 1;

    String oldRank = ranks[playerRankPos];
    String newRank = ranks[targetRankPos];

    double newRankPrice = ranksPrice.get(newRank);

    if (econ.getBalance(sender) < newRankPrice) {
      sender.sendMessage(prefix("Ranks") + "You don't have enough money to upgrade!");
      return;
    }

    econ.withdrawPlayer(sender, newRankPrice);

    perm.playerRemoveGroup(null, sender, oldRank);
    perm.playerAddGroup(null, sender, newRank);

    Bukkit.broadcastMessage(prefix("Ranks") + sender.getDisplayName() + ChatFormats.accentColor
        + " is now " + ChatFormats.baseColor + "a " + ChatFormats.accentColor + newRank
        + ChatFormats.baseColor + " !");

    TitleAPI.sendTitle(sender, ChatFormats.accentColor + "Promoted!",
        ChatFormats.baseColor + "You're now " + ChatFormats.accentColor + newRank);
  }

  private void ranks(Player sender) {
    int playerRankPos = getRankPosition(sender);

    sender.sendMessage(ChatFormats.line());

    for (int i = 1; i < ranks.length; i++)
      sender.sendMessage(" "
          + (playerRankPos >= i ? ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH
              : ChatFormats.accentColor)
          + ranks[i - 1]
          + (playerRankPos >= i ? ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH
              : ChatFormats.baseColor)
          + " -> "
          + (playerRankPos >= i ? ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH
              : ChatFormats.accentColor)
          + ranks[i]
          + (playerRankPos >= i ? ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH
              : ChatFormats.baseColor)
          + " : "
          + (playerRankPos >= i ? ChatColor.DARK_GRAY + "" + ChatColor.STRIKETHROUGH
              : ChatFormats.accentColor)
          + df.format(new BigDecimal(ranksPrice.get(ranks[i])).doubleValue()) + " Nu");

    sender.sendMessage(ChatFormats.line());
  }

  private int getRankPosition(String g) {
    for (int i = 0; i < ranks.length; i++)
      if (g.equalsIgnoreCase(ranks[i]))
        return i;

    return -1;
  }

  private int getRankPosition(Player p) {
    int pos;

    for (String g : perm.getPlayerGroups(p))
      if ((pos = getRankPosition(g)) != -1)
        return pos;

    return -1;
  }

}
