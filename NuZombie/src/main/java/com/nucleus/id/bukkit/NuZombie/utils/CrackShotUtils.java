/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.utils;

import com.shampaggon.crackshot.CSUtility;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.ArrayList;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class CrackShotUtils {
  private static ArrayList<String> weapons = new ArrayList<>();
  private static ArrayList<Integer> ammo_ids = new ArrayList<>();

  private static Map<Integer, String> ammos = new TreeMap<>();

  private static CSUtility cs;
  private static Random rand;

  static {
    cs = new CSUtility();
    rand = new Random();

    // Tier-1 Weapons
    addWeapons(new String[] {"Makarov", "M1911", "M9", "G17", "NVD", "Binoculars", "DMR", "CZ550",
        "M107", "AS50", "Winchester", "Remington", "LeeEnfield", "FNFAL", "AK-47", "Crossbow",
        "Twin-BarrelShotgun", "Flashbang", "M1014", "Magnum", "M16", "M24", "M4A1"}, 128);

    // Tier-2 Weapons
    addWeapons(new String[] {"DUALINFINITY", "FiveSeven", "AA12", "Barrett-50Cal", "Chainsaw",
        "G36C", "M4A1-Horo", "P90"}, 64);

    // Tier-3 Weapons
    addWeapons(new String[] {"MachineBow", "SKS", "Olympia", "M30", "HDNW", "SKS"}, 32);

    // Tier-4 Weapons
    addWeapons(new String[] {"40mm-Bofors", "ASS"}, 16);

    // Tier-5 Weapons
    addWeapons(new String[] {}, 8);

    // Tier-6 Weapons
    addWeapons(new String[] {"AKGK"}, 4);

    // Tier-7 Weapons
    addWeapons(new String[] {"M1014-Bynhildr"}, 2);

    // Jogmungard
    addWeapon("Jogmungard", 3);

    int[] ids = {361, 371, 295, 318, 266, 336};

    for (int x : ids) {
      ammo_ids.add(x);
    }

    ammos.put(361, "Pistol Ammo");
    ammos.put(318, "Rifle Ammo");
    ammos.put(336, "M30 Ammo");
    ammos.put(295, "Shotgun Ammo");
    ammos.put(371, "Sniper Rounds");
    ammos.put(266, "Autocannon Ammo");
  }

  public static ItemStack getRandomWeapon() {
    return cs.generateWeapon(weapons.get(rand.nextInt(weapons.size())));
  }

  public static ItemStack getRandomAmmo() {
    int n = rand.nextInt(101);

    int id = 0;

    if (n >= 0) {
      id = 371; // sniper
    }
    if (n >= 10) {
      id = 295; // shotgun
    }
    if (n >= 25) {
      id = 336; // m30
    }
    if (n >= 55) {
      id = 318; // rifle
    }
    if (n >= 72) {
      id = 361; // pistol
    }
    if (n >= 90) {
      id = 266; // autocannon
    }

    String name = ammos.get(id);

    ItemStack ammo = new ItemStack(Material.getMaterial(id), rand.nextInt(63) + 1);
    ItemMeta ammoMeta = ammo.getItemMeta();

    ammoMeta.setDisplayName(ChatColor.YELLOW + name);

    ammo.setItemMeta(ammoMeta);

    return ammo;
  }

  private static void addWeapon(String weaponTitle, int chance) {
    for (int i = 0; i <= chance; i++)
      weapons.add(weaponTitle);
  }

  private static void addWeapons(String[] weaponsTitle, int chance) {
    for (String s : weaponsTitle)
      addWeapon(s, chance);
  }
}
