/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.taxes;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.wargamer2010.signshop.events.SSPostTransactionEvent;

import com.Acrobot.ChestShop.Events.TransactionEvent;
import com.massivecraft.factions.event.LandClaimEvent;
import com.nucleus.id.bukkit.NuCommon.NuPluginBase;
import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;

import net.milkbowl.vault.economy.Economy;

public class ModuleTaxes extends Module {

  public static final double TAX = 0.1;
  public static final double CLAIM = 4;

  Economy econ;
  private FileConfiguration config;

  public ModuleTaxes() {
    super("Taxes", new String[] {"tax"});

    config = NuPluginBase.getInstance().getConfig();
  }

  @Override
  public void onEnable() {
    econ = this.plugin.getEconomy();

    config.addDefault("taxesBank.currentBalance", -4637.29);
    config.options().copyDefaults(true);
    NuPluginBase.getInstance().saveConfig();
  }

  @EventHandler
  public void onSignShopTransaction(SSPostTransactionEvent e) {
    if (!e.getOperation().contains("buy") || e.getOperation().startsWith("i"))
      return;

    double taxPrice = e.getPrice() * TAX;
    double withTax = e.getPrice() + taxPrice;

    e.getPlayer().sendMessage(ChatColor.GREEN + "10% Tax is applied to your purchase");
    e.getPlayer().sendMessage(taxPrice + " Nu will be charged for Tax");

    econ.withdrawPlayer(e.getPlayer().getPlayer(), taxPrice);
    addTaxBank(taxPrice);

    if (econ.getBalance(e.getPlayer().getPlayer()) < 0)
      e.getPlayer().sendMessage(ChatFormats.accentColor + "Your account is on the red");
  }

  @EventHandler
  public void onChestShopTransaction(TransactionEvent e) {
    double taxPrice = e.getPrice() * TAX;
    double withTax = e.getPrice() + taxPrice;

    e.getClient().sendMessage(prefix() + ChatColor.GREEN + "10% Tax is applied to your purchase");
    e.getClient().sendMessage(prefix() + taxPrice + " Nu will be charged for Tax");

    econ.withdrawPlayer(e.getClient().getPlayer(), taxPrice);
    addTaxBank(taxPrice);

    if (econ.getBalance(e.getClient()) < 0)
      e.getClient().sendMessage(prefix() + ChatFormats.accentColor + "Your account is on the red");
  }

  @EventHandler
  public void onLandClaim(LandClaimEvent e) {
    if (econ.getBalance(e.getPlayer()) < 0) {
      e.getPlayer().sendMessage(prefix() + ChatFormats.accentColor
          + "You can't claim this land, because your account is on the red ( < 0 )");
      e.getPlayer().sendMessage(prefix() + "You can get more money by trading gold to money on "
          + ChatFormats.accentColor + "/warp xchange");
      e.setCancelled(true);

      return;
    }

    e.getPlayer().sendMessage(prefix() + ChatFormats.accentColor + CLAIM + " Nu "
        + ChatFormats.baseColor + "has been taken from your account");

    econ.withdrawPlayer(e.getPlayer(), CLAIM);
    addTaxBank(CLAIM);

    if (econ.getBalance(e.getPlayer()) < 0)
      e.getPlayer()
          .sendMessage(prefix() + ChatFormats.accentColor + "Your account is on the red ( < 0 )");
  }

  @Override
  public void onCommand(Player sender, String cmd, String[] args) {
    if ((sender.hasPermission(Permissions.STAFF) && (args.length >= 1))) {
      if (args[0].equalsIgnoreCase("bank")) {
        double tax = getTaxBank();
        ChatColor color;

        if (tax > 0) {
          color = ChatColor.GREEN;
        } else {
          color = ChatFormats.accentColor;
        }

        sendMessage(sender, "Current balance is " + color + Math.round(getTaxBank()) + "Nu");
      }
      if (args[0].equalsIgnoreCase("transfer")) {
        if (args.length > 2) {
          if (transferToPlayer(this.getPlugin().getServer().getPlayer(args[1]),
              Integer.parseInt(args[2]))) {
          } else {
            sendMessage(sender,
                "Failed to transfer " + args[2] + "Nu to " + args[1] + " (not enough balance!)");
          }
        } else {
          sendMessage(sender, "Incorrect usage. /tax transfer <playerName> <amount>");
        }
      }
    } else {
      sendMessage(sender, "Current tax is " + Math.round(TAX * 100) + "%");
    }
  }

  private boolean transferToPlayer(Player player, int amount) {
    if (getTaxBank() > amount) {
      econ.depositPlayer(player, amount);
      addTaxBank(-amount);
      sendMessage(player, amount + "Nu has been successfully transferred into your account");
      return true;
    } else {
      return false;
    }
  }

  public double getTaxBank() {
    return config.getDouble("taxesBank.currentBalance");
  }

  public void setTaxBank(double tax) {
    config.set("taxesBank.currentBalance", tax);
    NuPluginBase.getInstance().saveConfig();
  }

  public void addTaxBank(double tax) {
    config.set("taxesBank.currentBalance", config.getDouble("taxesBank.currentBalance") + tax);
    NuPluginBase.getInstance().saveConfig();
  }


}
