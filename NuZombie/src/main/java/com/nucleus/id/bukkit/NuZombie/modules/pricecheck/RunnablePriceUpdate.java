/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.pricecheck;

import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class RunnablePriceUpdate implements Runnable {

  private Connection database;
  private List<ItemPrice> prices;

  public RunnablePriceUpdate(Connection database, List<ItemPrice> prices) {
    this.database = database;
    this.prices = prices;
  }

  @Override
  public void run() {
    prices.clear();

    try {
      PreparedStatement preparedStatement = database.prepareStatement("SELECT * FROM prices");

      ResultSet result = preparedStatement.executeQuery();

      while (result.next())
        prices.add(new ItemPrice(result.getString("Item_Name"), result.getDouble("Average"),
            result.getDouble("Median")));

      Bukkit.broadcastMessage(ChatFormats.prefixFormat("Economy") + " Price list updated");

      Bukkit.getOnlinePlayers().stream().forEach(
          (Player p) -> TitleAPI.sendActionBar(p, ChatFormats.baseColor + "Price list updated"));
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }

}
