/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.gameplay;


import com.nucleus.id.bukkit.NuCommon.formats.ChatFormats;
import com.nucleus.id.bukkit.NuCommon.modules.Module;
import com.nucleus.id.bukkit.NuCommon.utils.Permissions;
import com.nucleus.id.bukkit.NuCommon.utils.TitleAPI;

import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.event.player.PlayerTeleportEvent.TeleportCause;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

/**
 * ModuleGameplay is a module that adds some several modifications regarding the gameplay of the
 * player:
 * <ul>
 * <li>Prevent picking up items if player is not sneaking</li>
 * <li>Blinds and confuse player if the player falls</li>
 * <li>Disable drops from PIG_ZOMBIE, as it drops currency</li>
 * </ul>
 */
public class ModuleGameplay extends Module {

  final double MONSTER_RADIUS = 10;

  public ModuleGameplay() {
    super("Gameplay");
  }

  /**
   * Handles PlayerPickupItemEvent so the player can only pick up items when the player is sneaking
   *
   * @param e the event
   */
  @EventHandler
  public void pickupDisabler(PlayerPickupItemEvent e) {
    if (!e.getPlayer().isSneaking()) {
      e.setCancelled(true);

      TitleAPI.sendActionBar(e.getPlayer(), ChatFormats.baseColor + "You have to "
          + ChatFormats.accentColor + "sneak" + ChatFormats.baseColor + " to pickup items");
    }
  }

  /**
   * Handles EntityDamageEvent so the player gets several effects when fallen
   *
   * @param e the event
   */
  @EventHandler
  public void fallDamage(EntityDamageEvent e) {
    if (e.getEntity() instanceof Player) {
      Player p = (Player) e.getEntity();

      if (e.getCause() == EntityDamageEvent.DamageCause.FALL) {
        p.addPotionEffect(
            new PotionEffect(PotionEffectType.BLINDNESS, 20 * 8 * (int) e.getDamage() / 20, 2));
        p.addPotionEffect(
            new PotionEffect(PotionEffectType.CONFUSION, 20 * 8 * (int) e.getDamage() / 20, 2));
      }
    }
  }

  /**
   * Handles PlayerTeleportEvent so the player gets several effects and damage when teleporting with
   * an enderpearl
   * 
   * @param e the event
   */
  @EventHandler
  public void enderPearlDamage(PlayerTeleportEvent e) {
    Player p = e.getPlayer();

    if (e.getCause() == PlayerTeleportEvent.TeleportCause.ENDER_PEARL) {
      double distance = e.getFrom().distance(e.getTo());

      p.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 20 * 8 * (int) distance, 2));
      p.addPotionEffect(new PotionEffect(PotionEffectType.CONFUSION, 20 * 8 * (int) distance, 2));

      p.damage(p.getHealth() * 0.25);
    }
  }

  /**
   * Handles PlayerTeleport Event so the player cannot teleport when there's monsters nearby
   * 
   * @param e the event
   */
  @EventHandler
  public void denyTeleport(PlayerTeleportEvent e) {
    Player p = e.getPlayer();

    if (p.hasPermission(Permissions.STAFF) || e.getCause() != TeleportCause.COMMAND)
      return;

    boolean isMonsterNearby = p.getNearbyEntities(MONSTER_RADIUS, MONSTER_RADIUS, MONSTER_RADIUS)
        .stream().anyMatch(ent -> ent instanceof Monster);

    if (isMonsterNearby) {
      p.sendMessage(prefix("Server") + "You cannot teleport when there's monsters nearby");
      e.setCancelled(true);
    }
  }
}
