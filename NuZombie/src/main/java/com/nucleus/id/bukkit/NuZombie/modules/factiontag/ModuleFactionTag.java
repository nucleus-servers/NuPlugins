/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.factiontag;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.event.FPlayerJoinEvent;
import com.massivecraft.factions.event.FPlayerLeaveEvent;
import com.massivecraft.factions.event.FactionRelationEvent;
import com.massivecraft.factions.struct.Rel;
import com.nucleus.id.bukkit.NuCommon.modules.Module;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.kitteh.tag.AsyncPlayerReceiveNameTagEvent;
import org.kitteh.tag.TagAPI;

public class ModuleFactionTag extends Module {

  public ModuleFactionTag() {
    super("Faction Tag");
  }

  @EventHandler
  public void receiveTag(AsyncPlayerReceiveNameTagEvent e) {
    Player observer = e.getPlayer();
    Player observed = e.getNamedPlayer();

    FPlayer fPlayerObserver = FPlayers.i.get(observer);
    FPlayer fPlayerObserved = FPlayers.i.get(observed);

    Faction observedFaction = fPlayerObserved.getFaction();

    Rel relation = fPlayerObserver.getRelationTo(fPlayerObserved);

    if (observedFaction == Factions.i.getBestTagMatch("Wilderness"))
      return;

    e.setTag(relation.getColor() + fPlayerObserved.getRole().getPrefix() + observed.getName());
  }

  @EventHandler
  public void relationChange(FactionRelationEvent e) {
    Faction faction = e.getFaction();
    Faction targetFaction = e.getTargetFaction();

    refreshWholeFaction(faction);
    refreshWholeFaction(targetFaction);
  }

  @EventHandler
  public void joinFaction(FPlayerJoinEvent e) {
    Faction faction = e.getFaction();

    refreshWholeFaction(faction);
  }

  @EventHandler
  public void leaveFaction(FPlayerLeaveEvent e) {
    Faction faction = e.getFaction();

    refreshWholeFaction(faction);
  }

  private void refreshWholeFaction(Faction f) {
    f.getFPlayers().stream().forEach((fplayer) -> TagAPI.refreshPlayer(fplayer.getPlayer()));
  }

}
