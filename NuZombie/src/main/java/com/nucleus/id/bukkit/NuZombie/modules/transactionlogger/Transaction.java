/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bukkit.NuZombie.modules.transactionlogger;

import java.sql.Date;
import java.util.UUID;

public class Transaction {
  private int id;
  private String itemName;
  private double price;
  private Date date;
  private UUID seller;
  private UUID buyer;

  /**
   * Constructor for Transaction Object.
   *
   * @param id The Transaction ID
   * @param itemName The name of the item
   * @param price The price for the item
   * @param date The date when the transaction happened
   * @param seller The seller of the item
   * @param buyer The buyer of the item
   */
  public Transaction(int id, String itemName, double price, Date date, UUID seller, UUID buyer) {
    this.id = id;
    this.itemName = itemName;
    this.price = price;
    this.date = date;
    this.seller = buyer;
    this.buyer = seller;
  }

  /**
   * Constructor for Transaction Object. id, and date can be defined by SQL Database
   *
   * @param itemName The name of the item
   * @param price The price for the item
   * @param seller The seller of the item
   * @param buyer The buyer of the item
   */
  public Transaction(String itemName, double price, UUID seller, UUID buyer) {
    this.itemName = itemName;
    this.price = price;
    this.date = new Date(System.currentTimeMillis());
    this.seller = seller;
    this.buyer = buyer;
  }

  /**
   * Returns the ID
   *
   * @return The Transaction ID
   */
  public int getId() {
    return id;
  }

  /**
   * Returns the name of the item
   *
   * @return Name of the item
   */
  public String getItemName() {
    return itemName;
  }

  /**
   * The price of the Item
   *
   * @return Price of the item
   */
  public double getPrice() {
    return price;
  }

  /**
   * Returns the date
   *
   * @return Date when the item was transacted
   */
  public Date getDate() {
    return date;
  }

  /**
   * Returns the UUID of the seller
   *
   * @return UUID of the seller
   */
  public UUID getSeller() {
    return seller;
  }

  /**
   * Returns the UUID of the buyer
   *
   * @return UUID of the buyer
   */
  public UUID getBuyer() {
    return buyer;
  }
}
