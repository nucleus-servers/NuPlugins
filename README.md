# Discontinued: NuPlugins is discontinued, because Nucleus uses SpongeAPI and Lilypad

# NuPlugins
Plugins that powers Nucleus's Servers

## What are These?
* NuCommon : NuCommon is the shared Libraries and Modules between NuPlugins
* NuZombie : The Plugin that powers Project-Z
* NuBungee : The Plugin that powers Nucleus's BungeeCord Server

## Building
You need Java Development Kit version 8 and Gradle to build NuPlugins
* Open up your Terminal / Command Prompt
* Run `gradle build`
* If it fails to find dependencies, add it manually

## Installing
* Put the jar files into your server's plugins folder
* Start and Stop the server
* Configure the database settings
* Start it again
NOTE: We do **not** recommend **reloading** your server or the plugin it self.

## Contribution Guidelines
* Read CONTRIBUTING.md

## Contact
* Email : yukinagato@protonmail.com