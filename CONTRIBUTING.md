# Contribution Guide

## What you should use
* Google's Java Style
* Gradle
* Git Flow

## What we recommend to use
* IntelliJ IDEA
* Eclipse

## Commit Message Format ##
`[<Module / Server (Bungee/Common/Project-Z/Nations)>/<Module/Part>] <Changes>`

## What you shouldn't do
* Use JAR files as an dependency ( request Kaisar Arkhan to add it to Nucleus's Repo )
* Confusing Code ( Every Code should be readable and can be understood by other people )
* Multiple Changes in one commit
* Not Branching-out for changes ( This is why you should use Git Flow )