/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class HTTPUtils {

  private static final String USER_AGENT = "Mozilla/5.0";

  public static String sendGet(String url) throws Exception {
    URL u = new URL(url);
    HttpURLConnection conn = (HttpURLConnection) u.openConnection();

    conn.setRequestMethod("GET");

    conn.setRequestProperty("User-Agent", USER_AGENT);

    BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));

    String line;
    StringBuilder sb = new StringBuilder();

    while ((line = in.readLine()) != null)
      sb.append(line);

    in.close();

    return sb.toString();
  }

}
