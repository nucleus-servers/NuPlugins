/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;

public class PremiumUtil {

  private static HashMap<String, Boolean> premiumStatus = new HashMap<>();

  public static boolean checkPremium(String username) {
    if (premiumStatus.containsKey(username))
      return premiumStatus.get(username);

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(
          new URL("https://minecraft.net/haspaid.jsp?user=" + username).openStream()));

      String result = br.readLine();

      premiumStatus.put(username, Boolean.parseBoolean(result));

      return premiumStatus.get(username);
    } catch (IOException e1) {
      return false;
    }
  }
}
