/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.antibot.ipcheckers;

import com.nucleus.id.bungee.utils.HTTPUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IPCheckerStopForumSpam extends IPChecker {

  private String apiUrl = "http://www.stopforumspam.com/api?ip=";
  private String resultRegex = "(<appears>)(\\w+)(<\\/appears>)";

  @Override
  public boolean isProxy(String ip) {
    try {
      String result = HTTPUtils.sendGet(apiUrl + ip);

      Matcher m = Pattern.compile(resultRegex).matcher(result);

      if (m.find())
        if ((result = m.group(2)) == "yes")
          return true;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return false;
  }

}
