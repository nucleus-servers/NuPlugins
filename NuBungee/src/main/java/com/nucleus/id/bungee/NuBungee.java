/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee;

import com.google.common.io.ByteStreams;

import com.nucleus.id.bungee.listeners.ModuleCommandsListener;
import com.nucleus.id.bungee.managers.ModuleManager;
import com.nucleus.id.bungee.modules.antibot.ModuleAntiBot;
import com.nucleus.id.bungee.modules.antisteve.ModuleAntiSteve;
import com.nucleus.id.bungee.modules.connectmessage.ModuleConnectMessage;
import com.nucleus.id.bungee.modules.fun.ModuleFun;
import com.nucleus.id.bungee.modules.globalchat.ModuleGlobalChat;
import com.nucleus.id.bungee.modules.privatemessage.ModulePrivateMessage;
import com.nucleus.id.bungee.modules.slashserver.ModuleSlashServer;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Logger;

public class NuBungee extends Plugin {
  private static NuBungee instance;

  private final Class[] moduleClasses = new Class[] {ModuleGlobalChat.class, ModuleAntiBot.class,
      ModuleConnectMessage.class, ModulePrivateMessage.class, ModuleSlashServer.class,
      ModuleFun.class, ModuleAntiSteve.class};

  private final Class[] listenerClasses = new Class[] {ModuleCommandsListener.class};

  private Logger logger = getLogger();

  private Configuration config;
  private Connection conn;

  public static NuBungee getInstance() {
    return instance;
  }

  @Override
  public void onEnable() {
    instance = this;

    logger = this.getLogger();

    logger.info("--- Enabling NuBungee --- ");

    logger.info(">> Loading Configuration File ...");
    loadConfig();

    logger.info(">> Starting SQL Connection ...");
    if (!startDatabaseConnection())
      return;

    logger.info(">> Loading Modules ...");
    ModuleManager.loadModules(moduleClasses);

    logger.info(">> Enabling Modules ...");
    ModuleManager.enableAllModules();

    logger.info(">> Registering Listeners ...");
    registerListeners();
  }

  @Override
  public void onDisable() {
    logger.info("--- Disabling NuBungee ---");

    logger.info(">> Unloading Modules ...");
    ModuleManager.unloadAllModules();

    logger.info(">> Closing SQL Connection ...");
    stopDatabaseConnection();

    logger.info("--- NuBungee Disabled ---");
  }

  public Configuration getConfig() {
    return config;
  }

  private void loadConfig() {
    if (!this.getDataFolder().exists())
      this.getDataFolder().mkdir();

    File configFile = new File(getDataFolder(), "config.yml");

    if (!configFile.exists()) {
      try {
        configFile.createNewFile();
        try (InputStream in = getResourceAsStream("config.yml");
            OutputStream out = new FileOutputStream(configFile)) {
          ByteStreams.copy(in, out);
        }
      } catch (IOException e) {
        throw new RuntimeException("Unable to create configuration file ", e);
      }
    }

    try {
      config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
    } catch (IOException e) {
      throw new RuntimeException("Unable to load configuration file", e);
    }
  }

  // TODO: Change to a proper Plugin Disable Mechanic
  private boolean startDatabaseConnection() {
    if (config.getBoolean("mysql.use", false)) {
      // Use MySQL
      try {
        Class.forName("org.mysql.JDBC");
      } catch (ClassNotFoundException e) {
        logger.severe("Database driver error: " + e.getMessage());
      }

      try {
        conn = DriverManager.getConnection(
            "jdbc:mysql://" + config.getString("mysql.host") + ":" + config.getInt("mysql.port")
                + "/" + config.getString("mysql.database"),
            config.getString("mysql.username"), config.getString("mysql.password"));

        return true;
      } catch (SQLException e) {
        logger.severe("Error while starting Database Connection! SUICIDE IN PROGRESS !!");
        e.printStackTrace();
      }
    } else {
      // Use SQLite
      try {
        Class.forName("org.sqlite.JDBC");
      } catch (ClassNotFoundException e) {
        logger.severe("Database driver error:" + e.getMessage());
      }

      File dbFile = new File(getDataFolder(), "database.db");
      try {
        conn = DriverManager.getConnection("jdbc:sqlite:" + dbFile.getAbsolutePath());

        return true;
      } catch (SQLException e) {
        logger.severe("Error while starting Database Connection! SUiCIDE IN PROGRESS !!");
        e.printStackTrace();
      }
    }

    return false;
  }

  private void stopDatabaseConnection() {
    try {
      conn.close();
    } catch (SQLException ignored) {
    }
  }

  private void registerListeners() {
    for (Class<Listener> listenerClass : listenerClasses) {
      try {
        ProxyServer.getInstance().getPluginManager().registerListener(this,
            listenerClass.newInstance());
      } catch (Exception e) {
        logger.severe("An Exception occured while registering a listener " + e);
        e.printStackTrace();
      }
    }

  }

}
