/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.antisteve;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.modules.Module;
import com.nucleus.id.bungee.utils.NumberUtils;
import com.nucleus.id.bungee.utils.PremiumUtil;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.logging.Level;

import skinsrestorer.shared.api.SkinsRestorerAPI;
import skinsrestorer.shared.utils.SkinFetchUtils;

public class ModuleAntiSteve extends Module {

  String[] skins = {"goodthief", "kmlkmljkl", "csd", "Bletotum", "Sukor", "Hoodoo456", "Zakhep",
      "Alienation25", "InsanityMan", "Tiwilizer5953", "Milkcooks", "adnan252", "AleksKavli",
      "SiskoUrso", "adzicents", "Prusselusken", "Saxton_Hale", "TheBoatman", "ControlFreak92",
      "interwolf", "Nukem", "BMCha", "Metroidling", "Nevec", "Cellular", "Jojje94", "flubbernugget",
      "theseltsamone", "Kolton", "Hai2joo", "Goofa", "jackcaver", "corbin8667"};


  public ModuleAntiSteve() {
    super("Anti Steve");
  }

  @EventHandler(priority = EventPriority.HIGH)
  public void join(ServerConnectedEvent e) {

    String name = e.getPlayer().getName();

    String skin = skins[NumberUtils.getRandomNumber(0, skins.length)];

    ProxyServer.getInstance().getScheduler().runAsync(plugin, () -> {
      boolean premium = PremiumUtil.checkPremium(name);

      log(Level.INFO, name + " is " + (premium ? "premium" : "cracked / offline"));

      if (!premium && SkinsRestorerAPI.getSkinName(name).equalsIgnoreCase(name)) {
        try {
          SkinsRestorerAPI.setSkin(name, skin);
          e.getPlayer().sendMessage(new TextComponent(prefix() + "Hello, Your skin has been set to "
              + ChatFormats.accentColor + skin + ChatFormats.baseColor + ". You may need to "
              + ChatFormats.accentColor + "re-login" + ChatFormats.baseColor + " to view changes"));
          e.getPlayer()
              .sendMessage(new TextComponent(
                  prefix() + "If you want to change your skin " + ChatFormats.accentColor + "freely"
                      + ChatFormats.baseColor + ", Please " + ChatFormats.accentColor + "Donate!"));
        } catch (SkinFetchUtils.SkinFetchFailedException ex) {
          log(Level.WARNING, "Failed to fetch skin : " + skin);
        }
      }
    });

  }


}
