/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.antibot;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.modules.Module;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.PlayerHandshakeEvent;
import net.md_5.bungee.event.EventHandler;
import net.md_5.bungee.event.EventPriority;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ModuleAntiBot extends Module {

  public static List<String> badIPs = new ArrayList<>();
  private final String badIPKickMessage =
      ChatFormats.accentColor + "" + ChatColor.BOLD + "BAD IP DETECTED\n" + ChatColor.WHITE
          + "For Safety Reasons, Proxies are " + ChatFormats.accentColor + "banned"
          + ChatColor.WHITE + " in Nucleus\n" + ChatFormats.accentColor
          + "Please turn off your Proxy\n" + ChatColor.WHITE + "- Nice Try, Anti-Bot -";
  private final String tmcKickMessage = ChatFormats.accentColor + "" + ChatColor.BOLD
      + "TOO MANY CLIENTS\n" + ChatColor.WHITE + "You have too many " + ChatFormats.accentColor
      + "unreliable clients\n" + ChatFormats.accentColor + "Please disconnect immediately";
  private HashMap<String, Long> lastConnect = new HashMap<>();
  private HashMap<String, Integer> attempts = new HashMap<>();

  public ModuleAntiBot() {
    super("Anti Bot");
  }

  @EventHandler(priority = EventPriority.HIGH)
  public void onConnect(PlayerHandshakeEvent e) {
    String ip = e.getConnection().getAddress().getAddress().toString().substring(1);

    if (badIPs.contains(ip))
      e.getConnection().disconnect(new TextComponent(badIPKickMessage));
    else
      ProxyServer.getInstance().getScheduler().runAsync(plugin, new IPCheckRunnable(ip));

    if (lastConnect.containsKey(ip))
      if (System.currentTimeMillis() - lastConnect.get(ip) < 1.5 * 1000)
        disconnectSameIP(ip);
      else
        lastConnect.put(ip, System.currentTimeMillis());
    else
      lastConnect.put(ip, System.currentTimeMillis());
  }

  private void disconnectSameIP(String ip) {
    for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers()) {
      String ip2 = p.getAddress().getAddress().toString().substring(1);

      if (ip.equals(ip2))
        p.disconnect(new TextComponent(tmcKickMessage));
    }
  }
}
