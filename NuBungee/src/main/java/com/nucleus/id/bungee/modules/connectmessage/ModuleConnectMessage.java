/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.connectmessage;

import com.nucleus.id.bungee.modules.Module;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.ServerConnectedEvent;
import net.md_5.bungee.api.event.ServerDisconnectEvent;
import net.md_5.bungee.api.event.ServerSwitchEvent;
import net.md_5.bungee.event.EventHandler;

public class ModuleConnectMessage extends Module {

  public ModuleConnectMessage() {
    super("Connect Message");
  }

  @EventHandler
  public void connectMessage(ServerConnectedEvent e) {
    ProxyServer.getInstance().broadcast(new TextComponent(
        prefix("Network") + e.getPlayer().getName() + " joined Nucleus's Server."));
  }

  @EventHandler
  public void disconnectMessage(ServerDisconnectEvent e) {
    ProxyServer.getInstance().broadcast(
        new TextComponent(prefix("Network") + e.getPlayer().getName() + " left Nucleus's Server."));
  }

  @EventHandler
  public void moveMessage(ServerSwitchEvent e) {
    ProxyServer.getInstance()
        .broadcast(new TextComponent(prefix("Network") + e.getPlayer().getName() + " switched to "
            + e.getPlayer().getServer().getInfo().getName()));
  }

}
