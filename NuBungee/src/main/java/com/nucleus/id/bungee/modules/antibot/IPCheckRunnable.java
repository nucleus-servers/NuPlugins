/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.antibot;

import com.nucleus.id.bungee.modules.antibot.ipcheckers.IPChecker;
import com.nucleus.id.bungee.modules.antibot.ipcheckers.IPCheckerShroomery;
import com.nucleus.id.bungee.modules.antibot.ipcheckers.IPCheckerStopForumSpam;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class IPCheckRunnable implements Runnable {

  private IPChecker[] ipCheckers =
      new IPChecker[] {new IPCheckerStopForumSpam(), new IPCheckerShroomery()};

  private String ip;

  public IPCheckRunnable(String ip) {
    this.ip = ip;
  }

  @Override
  public void run() {
    boolean isProxy = false;

    for (IPChecker ipc : ipCheckers) {
      if (ipc.isProxy(ip)) {
        isProxy = true;
        break;
      }
    }

    if (!isProxy)
      return;

    ModuleAntiBot.badIPs.add(ip);

    int i = 0;
    for (ProxiedPlayer p : ProxyServer.getInstance().getPlayers())
      if (p.getAddress().getAddress().toString().substring(1).equals(ip))
        i++;

    if (i + 1 >= 5)
      ModuleAntiBot.badIPs.add(ip);
  }

}
