/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules;


import com.nucleus.id.bungee.NuBungee;
import com.nucleus.id.bungee.formats.ChatFormats;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Listener;

import java.util.Random;
import java.util.logging.Level;

import static com.nucleus.id.bungee.formats.ChatFormats.prefixFormat;

public abstract class Module implements Listener {

  protected final String chatPrefix =
      ChatFormats.accentColor + "Nucleus-ZA" + ChatFormats.baseColor + ">" + ChatColor.RESET + " ";
  protected final Random rand = new Random();
  protected NuBungee plugin = NuBungee.getInstance();
  protected String name = "NO_NAME";
  protected String[] commands = new String[0];
  protected boolean enabled = false;

  public Module(String name) {
    this.name = name;
  }

  public Module(String name, String[] commands) {
    this.name = name;
    this.commands = commands;
  }

  /**
   * This Method triggers when the module is enabling
   */
  public void onEnable() {}

  /**
   * This Method triggers when the module is disabling
   */
  public void onDisable() {}

  /**
   * This Method triggers when the player uses a command from this module
   *
   * @param sender The Player who sent the command
   * @param cmd The Command that is being sent
   * @param args The Arguments for the command
   */
  public void onCommand(ProxiedPlayer sender, String cmd, String[] args) {}

  /**
   * Returns The Module's Name
   *
   * @return The Name of this module
   */
  public String getName() {
    return name;
  }

  /**
   * Sets The Module's Name
   */
  protected void setName(String name) {
    this.name = name;
  }

  /**
   * Returns an Array of String filled with commands that are accepted by this module
   *
   * @return An Array of Commands that are accepted by this module
   */
  public String[] getCommands() {
    return this.commands;
  }

  public boolean isEnabled() {
    return enabled;
  }

  public void setEnabled(boolean enabled) {
    this.enabled = enabled;
  }

  protected String prefix(String text) {
    return prefixFormat(text);
  }

  protected String prefix() {
    return prefix(name);
  }

  protected void log(Level l, String msg) {
    plugin.getLogger().log(l, "[" + name + "] " + msg);
  }

  protected NuBungee getPlugin() {
    return plugin;
  }
}
