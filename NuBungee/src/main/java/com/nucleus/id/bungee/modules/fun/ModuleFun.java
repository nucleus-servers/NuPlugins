/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.fun;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.modules.Module;
import com.nucleus.id.bungee.utils.NumberUtils;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ModuleFun extends Module {

  String name = "Inami-chan";

  String[] answer = new String[] {"Yes", "No", "May be", "I think you shouldn't",
      "I think you should", "I don't know", "Probably", "Sure", "Why not?", "Well...", "Of course",
      "Of course not", "May be No is the best answer", "Why do you event think about it?",
      "Kill yourself", "Hell No.", "Hell yeah!", "It's better to say yes than no",
      "It's better to say no than yes", "I think you should think about it",
      "I'm not sure what to think about this", "I don't get it", "You should think about it more",
      "Well.."};

  String[] love = new String[] {"gives %p a bear hug", "d-doesn't like %p, you b-baka!",
      "lovingly eviscerates %p.", "cooks a romantic dinner for %p.",
      "watches the stars under the moonlight with %p.",
      "sits next to %p, making no eye contact whatsoever.",
      "shyly approaches %p and mutters: \"c-can I hold your hand?\"", "plays a lullaby for %p",
      "sings %p a love ballad.", "dances a slow waltz with %p.", "gives %p a delicious steak.",
      "bakes a cake for %p", "hugs %p in a loving manner.", "nibbles on %p's left ear.",
      "reads %p's future.", "whispers in %p's ear."};

  String[] hate = new String[] {"slaps %p in the face.", "kicks %p in the balls/tits.",
      "pulls %p's hair.", "spits on %p's face.", "makes %p breathe fire with napalm.",
      "poisons %p's food.", "staples %p's ears.", "gives %p the middle finger",
      "eviscerates %p in a non-loving manner.",
      "calls the police and tells them %p raped her cousin", "gives %p bad news",
      "throws rotten flesh on %p", "thinks %p has cooties"};

  long lastTime = 0;
  long waitTime = 1000L * 5L;

  public ModuleFun() {
    super("Fun", new String[] {"decide", "dice", "love", "hate"});
  }

  @Override
  public void onCommand(ProxiedPlayer p, String cmd, String[] args) {
    if (!(System.currentTimeMillis() - lastTime >= waitTime)) {
      p.sendMessage(new TextComponent(prefix() + "You're under a cooldown"));
      return;
    }

    StringBuilder sb = new StringBuilder();

    for (String s : args)
      sb.append(s + " ");

    broadcast(p.getDisplayName() + ChatFormats.baseColor + " : " + ChatFormats.accentColor + "/"
        + cmd + " " + sb.toString());

    if (cmd.equalsIgnoreCase("decide")) {
      decide(p, sb.toString().substring(0, sb.toString().length() - 1));
    } else if (cmd.equalsIgnoreCase("dice")) {
      dice(p, sb.toString().substring(0, sb.toString().length() - 1));
    } else if (cmd.equalsIgnoreCase("love")) {
      love(p, sb.toString().substring(0, sb.toString().length() - 1));
    } else if (cmd.equalsIgnoreCase("hate")) {
      hate(p, sb.toString().substring(0, sb.toString().length() - 1));
    }

    lastTime = System.currentTimeMillis();
  }

  public void decide(ProxiedPlayer p, String args) {
    String[] choices = args.split("( or |\\||,| atau )");

    if (choices.length == 1) {
      broadcastName(answer[NumberUtils.getRandomNumber(0, answer.length - 1)]);
    } else {
      broadcastName(choices[NumberUtils.getRandomNumber(0, choices.length - 1)]);
    }
  }

  public void dice(ProxiedPlayer p, String args) {
    broadcastName("The dice says " + NumberUtils.getRandomNumber(1, 6));
  }

  public void love(ProxiedPlayer p, String args) {
    if (args.length() == 0)
      args = p.getDisplayName();
    else if (ProxyServer.getInstance().getPlayer(args) != null)
      args = ProxyServer.getInstance().getPlayer(args).getDisplayName();

    broadcast(ChatFormats.accentColor + name + ChatFormats.baseColor + " "
        + love[NumberUtils.getRandomNumber(0, love.length - 1)].replace("%p",
            args + ChatFormats.baseColor));
  }

  public void hate(ProxiedPlayer p, String args) {
    if (args.length() == 0)
      args = p.getDisplayName();
    else if (ProxyServer.getInstance().getPlayer(args) != null)
      args = ProxyServer.getInstance().getPlayer(args).getDisplayName();

    broadcast(ChatFormats.accentColor + name + ChatFormats.baseColor + " "
        + hate[NumberUtils.getRandomNumber(0, hate.length - 1)].replace("%p",
            args + ChatFormats.baseColor));
  }

  private void broadcast(String msg) {
    ProxyServer.getInstance().broadcast(new TextComponent(ChatFormats.baseColor + "* " + msg));
  }

  private void broadcastName(String msg) {
    broadcast(
        ChatFormats.accentColor + name + ChatFormats.baseColor + ": " + ChatColor.WHITE + msg);
  }
}
