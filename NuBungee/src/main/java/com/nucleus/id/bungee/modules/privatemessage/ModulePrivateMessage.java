/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.privatemessage;

import com.google.common.base.Optional;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.modules.Module;
import com.nucleus.id.bungee.utils.UsefulRegex;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ModulePrivateMessage extends Module {

  private final String format = "&7[&c%s &7-> &c%r&7] &r%m";
  private Map<UUID, UUID> lastPerson = new HashMap<>();

  public ModulePrivateMessage() {
    super("Private Message",
        new String[] {"pm", "msg", "w", "whisper", "message", "tell", "m", "t", "r", "reply",
            "essentials:pm", "essentials:msg", "essentials:w", "essentials:whisper",
            "essentials:message", "essentials:tell", "essentials:m", "essentials:t", "essentials:r",
            "essentials:reply"});
  }

  @Override
  public void onCommand(ProxiedPlayer from, String cmd, String[] args) {
    boolean pm = cmd.equalsIgnoreCase("r") || cmd.equalsIgnoreCase("reply");
    Optional<ProxiedPlayer> toOptional;

    if (pm) {
      if (args.length > 0) {
        if (lastPerson.containsKey(from.getUniqueId())) {
          toOptional =
              Optional.of(ProxyServer.getInstance().getPlayer(lastPerson.get(from.getUniqueId())));

          if (toOptional.isPresent()) {
            sendMessage(from, toOptional.get(), buildMessage(args, 0));
          } else {
            from.sendMessage(new TextComponent(
                prefix("PM") + ChatFormats.accentColor + "That person is not online, right now"));
          }
        } else {
          from.sendMessage(new TextComponent(
              prefix("PM") + ChatFormats.accentColor + "You have nobody to reply to"));
        }
      } else {
        from.sendMessage(new TextComponent(
            prefix("PM") + "Usage: " + ChatFormats.accentColor + "/" + cmd + " <message>"));
      }
    } else if (args.length >= 2) {
      toOptional = Optional.of(ProxyServer.getInstance().getPlayer(args[0]));

      if (toOptional.isPresent()) {
        if (!toOptional.get().equals(from)) {
          sendMessage(from, toOptional.get(), buildMessage(args, 1));
        } else {
          from.sendMessage(new TextComponent(
              prefix("PM") + ChatFormats.accentColor + "You can't send messages to your self"));
        }
      } else {
        from.sendMessage(new TextComponent(
            prefix("PM") + ChatFormats.accentColor + "That person is not online, right now"));
      }
    } else {
      from.sendMessage(new TextComponent(prefix("PM") + "Usage: " + ChatFormats.accentColor + "/"
          + cmd + " <destination> <message>"));
    }
  }


  private void sendMessage(ProxiedPlayer from, ProxiedPlayer to, String message) {
    from.sendMessage(new TextComponent(formatMessage("You", to.getDisplayName(), message)));
    to.sendMessage(new TextComponent(formatMessage(from.getDisplayName(), "You", message)));

    lastPerson.put(from.getUniqueId(), to.getUniqueId());
    lastPerson.put(to.getUniqueId(), from.getUniqueId());
  }

  private String formatMessage(String sender, String receiver, String message) {
    return format.replaceAll(UsefulRegex.mcFormatRegex, "\u00A7$2").replaceAll("%s", sender)
        .replaceAll("%r", receiver).replaceAll("%m", message);
  }

  private String buildMessage(String[] args, int from) {
    StringBuilder sb = new StringBuilder();

    for (int i = from; i < args.length; i++) {
      sb.append(args[i]);

      if (i != args.length - 1)
        sb.append(" ");
    }

    return sb.toString();
  }


}
