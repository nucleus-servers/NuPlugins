/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.antibot.ipcheckers;

import com.nucleus.id.bungee.utils.HTTPUtils;

public class IPCheckerShroomery extends IPChecker {

  private String apiUrl = "http://www.shroomery.org/ythan/proxycheck.php?ip=";

  @Override
  public boolean isProxy(String ip) {
    try {
      String result = HTTPUtils.sendGet(apiUrl + ip);

      if (result == "Y")
        return true;

    } catch (Exception e) {
      e.printStackTrace();
    }

    return false;
  }

}
