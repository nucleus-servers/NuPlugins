/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.globalchat;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.modules.Module;

import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.event.PluginMessageEvent;
import net.md_5.bungee.event.EventHandler;

public class ModuleGlobalChat extends Module {

  public ModuleGlobalChat() {
    super("Global Chat");
  }

  @EventHandler
  public void listen(PluginMessageEvent e) {
    if (e.getTag().equalsIgnoreCase("BungeeCord")) {
      ByteArrayDataInput in = ByteStreams.newDataInput(e.getData());

      if (in.readUTF().equalsIgnoreCase("Nucleus"))
        if (in.readUTF().equalsIgnoreCase("Chat"))
          ProxyServer.getInstance()
              .broadcast(new TextComponent(ChatFormats.prefixFormat(ProxyServer.getInstance()
                  .getPlayer(e.getReceiver().toString()).getServer().getInfo().getName())
              + in.readUTF()));
    }
  }
}
