/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.modules.slashserver;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.modules.Module;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class ModuleSlashServer extends Module {

  public ModuleSlashServer() {
    // Why not? :^)
    super("Slash Server");
  }

  @Override
  public void onCommand(ProxiedPlayer sender, String cmd, String[] args) {
    if (sender.getServer().getInfo() == ProxyServer.getInstance().getServerInfo(cmd)) {
      sender.sendMessage(new TextComponent(
          prefix("NuBungee") + ChatFormats.accentColor + "You're already on that server"));
      return;
    }

    sender.connect(ProxyServer.getInstance().getServerInfo(cmd));
    sender.sendMessage(
        new TextComponent(prefix("NuBungee") + ChatColor.GREEN + "Connecting to " + cmd + " ..."));
  }

  @Override
  public String[] getCommands() {
    return ProxyServer.getInstance().getServers().keySet()
        .toArray(new String[ProxyServer.getInstance().getServers().size()]);
  }
}
