/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.listeners;

import com.nucleus.id.bungee.formats.ChatFormats;
import com.nucleus.id.bungee.managers.ModuleManager;
import com.nucleus.id.bungee.modules.Module;

import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ModuleCommandsListener implements Listener {

  @EventHandler
  public void onCommand(ChatEvent e) {
    if (!e.getMessage().startsWith("/"))
      return;

    String[] messages = e.getMessage().split(" ");

    List<String> argList = new ArrayList<>(Arrays.asList(messages));
    argList.remove(0);

    String[] args = argList.toArray(new String[argList.size()]);

    for (Module module : ModuleManager.getEnabledModules()) {
      for (String command : module.getCommands()) {
        if (command.equalsIgnoreCase(messages[0].substring(1))) {
          ProxiedPlayer p = (ProxiedPlayer) e.getSender();
          try {

            module.onCommand(p, command, args);
          } catch (Exception ex) {
            ex.printStackTrace();

            p.sendMessage(new TextComponent(ChatFormats.prefixFormat("NuBungee")
                + ChatFormats.accentColor
                + "An Error has occured while running this command. Please report to the Administator."));
          }
          e.setCancelled(true);
          return;
        }
      }
    }
  }

}
