/*
 * This file is part of NuPlugins.
 *
 * NuPlugins is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * NuPlugins is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with NuPlugins.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.nucleus.id.bungee.formats;

import net.md_5.bungee.api.ChatColor;

public class ChatFormats {
  public static ChatColor baseColor = ChatColor.GRAY;
  public static ChatColor accentColor = ChatColor.RED;

  public static String prefixFormat(String s) {
    return accentColor + s + baseColor + "> ";
  }

  public static String line() {
    return baseColor + "" + ChatColor.STRIKETHROUGH
        + "-----------------------------------------------------";
  }
}
